﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mainmap : MonoBehaviour
{
    public Player player;
    public GameObject mainMap;

    public float unitViewRadious = 0.2f;

    Texture2D fogTexture;
    Color[] colormap;
    Material mat;
    Camera mainMapCamera;

    public void Start()
    {
        mainMapCamera = gameObject.GetComponent<Camera>();
        ResetFog();
    }

    public void ResetFog()
    {
        RectTransform mapTrans = mainMap.GetComponent<RectTransform>();
        fogTexture = new Texture2D((int)mapTrans.rect.width, (int)mapTrans.rect.height);
        colormap = new Color[(int)mapTrans.rect.width * (int)mapTrans.rect.height];

        //fogTexture = new Texture2D((int)mainMapCamera.orthographicSize, (int)mainMapCamera.orthographicSize);
        //colormap = new Color[(int)mainMapCamera.orthographicSize * (int)mainMapCamera.orthographicSize];

        for (int idx = 0; idx < colormap.GetLength(0); idx++)
        {
            //print(idx / (float)colormap.GetLength(0));
            colormap[idx] = new Color(1, 1, 0, 0);
        }

        //fogTexture.filterMode = FilterMode.Point;
        //fogTexture.wrapMode = TextureWrapMode.Clamp;

        mat = mainMap.GetComponent<RawImage>().material;
        mat.SetTexture("_Mask", fogTexture);
    }

    void DrawCircle(Vector2 coord, int r)
    {
        for (int x = -r; x <= r; x++)
        {
            for (int y = -r; y <= r; y++)
            {
                if (x * x + y * y <= r * r)
                {
                    int drawX = (int)coord.x + x;
                    int drawY = (int)coord.y + y;
                    if (drawX >= 0 && drawX < fogTexture.width && drawY >=0 && drawY < fogTexture.height)
                    {
                        float a = Mathf.Lerp(1, 0, Mathf.Clamp(10f * ((float)(x * x + y * y) / (r * r) - 0.65f), 0f, 1f));
                        colormap[fogTexture.width * drawY + drawX] = new Color(1, 1, 1, Mathf.Max(a, colormap[fogTexture.width * drawY + drawX].a));
                    }
                }
            }
        }
    }

    void FixedUpdate()
    {
        if(player != null)
        {
            Vector3 cachePos = mainMapCamera.WorldToViewportPoint(player.transform.position);
            cachePos.x *= fogTexture.width;
            cachePos.y *= fogTexture.height;
            DrawCircle(cachePos, (int)(unitViewRadious * Mathf.Min(fogTexture.width, fogTexture.height)));
            //print((int)(unitViewRadious * Mathf.Min(fogTexture.width, fogTexture.height)));
            /*for (int idx = 0; idx < colormap.GetLength(0)/2; idx++)
            {
                //print(idx / (float)colormap.GetLength(0));
                colormap[idx] = new Color(1, 0, 1, 1);
            }*/

            fogTexture.SetPixels(colormap);
            fogTexture.Apply();
            //testsprit = Sprite.Create(fogTexture,testsprit.rect,testsprit.pivot);
        }
    }
}
