﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    public Player player;

    public float smoothTime;

    Vector3 targetPos;
    Vector3 cameraSmoothVelocity;

    void FixedUpdate()
    {
        if(player != null)
        {
            targetPos = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
            transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref cameraSmoothVelocity, smoothTime);
        }
    }
}
