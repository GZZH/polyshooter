﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour
{
    public Player player;
    public Image fadePlane;
    public Color gameoverBackground;
    public GameObject gameoverUI;
    public Text ammoInGun;
    public Text ammoRemain;
    public Text healthText;
    public Text shieldText;
    public GameObject mainMap;
    public GameObject miniMap;

    bool mapState;

    void Start ()
    {
        player.OnDeath += OnGameOver;
	}
	
	void OnGameOver ()
    {
        Cursor.visible = true;
        StartCoroutine(Fade(Color.clear, gameoverBackground, 1.5f));
        gameoverUI.SetActive(true);

    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    IEnumerator Fade (Color from, Color to, float fadeTime)
    {
        float fadeSpeed = 1 / fadeTime;
        float percent = 0;

        while(percent < 1)
        {
            percent += Time.deltaTime * fadeSpeed;
            fadePlane.color = Color.Lerp(from, to, percent);
            yield return null;
        }
    }

    public void SwitchMap(bool state)
    {
        if(mapState != state)
        {
            miniMap.SetActive(!state);
            mainMap.SetActive(state);
            mapState = state;
        }
    }

    //UI input
    public void StartNewGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void Update()
    {
        Vector2 ammoInfo = player.GetAmmoInfo();
        if (ammoInfo.x == -1)
        {
            ammoInGun.text = "∞";
        }
        else
        {
            ammoInGun.text = "" + ammoInfo.x;
        }
        if (ammoInfo.y == -1)
        {
            ammoRemain.text = "∞";
        }
        else
        {
            ammoRemain.text = "" + ammoInfo.y;
        }
        if(player != null)
        {
            Vector2 healthInfo = player.GetHealthInfo();
            healthText.text = "" + healthInfo.x;
            shieldText.text = "" + healthInfo.y;
        }
        else
        {
            healthText.text = "0";
            shieldText.text = "0";
        }
    }
}
