﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHair : MonoBehaviour
{
    public Crossleg[] crossLegs;

    public LayerMask targetMask;
    public Color highLightColor;
    public Vector2 expanRange;

    public float crossHairScale;
    public float stableTime = 0.05f;

    Material[] crossMat;
    Color originalColor;

    float currentPrecent = 1f;
    float targetPrecent = 1f;
    float stableSmoothVelocity;


    void Start ()
    {
        Cursor.visible = false;
        crossMat = new Material[crossLegs.Length];
        transform.localScale = Vector3.one * crossHairScale;
        for (int i = 0; i < crossLegs.Length; i++)
        {
            crossLegs[i].Start();
            crossMat[i] = crossLegs[i].crossLeg.GetComponent<Renderer>().material;
            originalColor = crossMat[i].color;
        }
    }
	
	public void SetExpandPrecent(float precent)
    {
        targetPrecent = precent;
        //targetPrecent = Mathf.Clamp(targetPrecent, expanRange.x, expanRange.y);
	}

    public void DetectTargets(Ray ray)
    {
        Color tempColor;
        if (Physics.Raycast(ray, 100, targetMask))
        {
            tempColor = highLightColor;
        }
        else
        {
            tempColor = originalColor;
        }
        for (int i = 0; i < crossMat.Length; i++)
        {
            crossMat[i].color = tempColor;
        }
    }

    public void Update()
    {
        currentPrecent = Mathf.SmoothDamp(currentPrecent, targetPrecent, ref stableSmoothVelocity, stableTime);
        for (int i = 0; i < crossLegs.Length; i++)
        {
            crossLegs[i].SetPrecent(currentPrecent);
        }
    }

    [System.Serializable]
    public class Crossleg
    {
        public Transform crossLeg;

        public int loc;

        float startPos;

        public void Start()
        {
            startPos = CrossLegPos;
        }

        public void SetPrecent(float precent)
        {
            CrossLegPos = startPos * precent;
        }

        public float CrossLegPos
        {
            get
            {
                if (loc == 0)
                {
                    return crossLeg.localPosition.x;
                }
                else if (loc == 1)
                {
                    return crossLeg.localPosition.y;
                }
                else if (loc == 2)
                {
                    return -crossLeg.localPosition.x;
                }
                else
                {
                    return -crossLeg.localPosition.y;
                }
            }
            set
            {
                if (loc == 0)
                {
                    crossLeg.localPosition = new Vector3(value, 0, 0);
                }
                else if (loc == 1)
                {
                    crossLeg.localPosition = new Vector3(0, value, 0);
                }
                else if (loc == 2)
                {
                    crossLeg.localPosition = new Vector3(-value, 0, 0);
                }
                else
                {
                    crossLeg.localPosition = new Vector3(0, -value, 0);
                }
            }
        }
    }
}
