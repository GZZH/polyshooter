﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    public Vector2[] resolutions;

    public GameObject mainMenuHolder;
    public GameObject optionsMenuHolder;

    public Slider[] volSliders;
    public Dropdown languageDropDown;
    public Dropdown resolutionDropDown;

    public Toggle fullscreenToggle;

    private void Start()
    {
        print("menu start-------------------------------");
        for (int idx = 0; idx < resolutions.Length; idx ++)
        {
            Dropdown.OptionData tempData = new Dropdown.OptionData();
            tempData.text = "" + resolutions[idx].x + "*" + resolutions[idx].y;
            resolutionDropDown.options.Add(tempData);
        }

        foreach(string option in Localizer.instance.GetLocalizedLanguageOptions())
        {
            Dropdown.OptionData tempData = new Dropdown.OptionData();
            tempData.text = option;
            languageDropDown.options.Add(tempData);
        }

        volSliders[0].value = PlayerPrefs.GetFloat("master vol", 1f);
        volSliders[1].value = PlayerPrefs.GetFloat("music vol", 1f);
        volSliders[2].value = PlayerPrefs.GetFloat("sfx vol", 1f);

        languageDropDown.value = PlayerPrefs.GetInt("language", 0);
        //SetLanguage();
        //languageDropDown.RefreshShownValue();

        resolutionDropDown.value = PlayerPrefs.GetInt("resolution", 0);
        //SetResolution();
        //resolutionDropDown.RefreshShownValue();

        fullscreenToggle.isOn = (PlayerPrefs.GetInt("fullscreen", 0) == 1)? true: false;
    }

    public void Play ()
    {
        SceneManager.LoadScene("Main");
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void OptionsMenu()
    {
        mainMenuHolder.SetActive(false);
        optionsMenuHolder.SetActive(true);
    }

    public void MainMenu()
    {
        mainMenuHolder.SetActive(true);
        optionsMenuHolder.SetActive(false);
    }

    public void SetResolution()
    {
        Screen.SetResolution((int)resolutions[resolutionDropDown.value].x, (int)resolutions[resolutionDropDown.value].y, false);
        PlayerPrefs.SetInt("resolution", resolutionDropDown.value);
        PlayerPrefs.Save();
    }

    public void SetLanguage()
    {
        Localizer.instance.UpdateLanguage(languageDropDown.value);
        PlayerPrefs.SetInt("language", languageDropDown.value);
        PlayerPrefs.Save();
    }

    public void SetFullscreen(bool isFullscreen)
    {
        resolutionDropDown.interactable = !isFullscreen;
        if(isFullscreen)
        {
            Resolution[] allResolutions = Screen.resolutions;
            Screen.SetResolution(allResolutions[allResolutions.Length - 1].width, allResolutions[allResolutions.Length - 1].height, true);
        }
        else
        {
            Screen.SetResolution((int)resolutions[resolutionDropDown.value].x, (int)resolutions[resolutionDropDown.value].y, false);
        }
        PlayerPrefs.SetInt("fullscreen", isFullscreen ? 1:0);
        PlayerPrefs.Save();
    }

    public void SetMasterVol(float value)
    {
        AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Master);
        PlayerPrefs.SetFloat("master vol", value);
        PlayerPrefs.Save();
    }

    public void SetMusicVol(float value)
    {
        AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Music);
        PlayerPrefs.SetFloat("music vol", value);
        PlayerPrefs.Save();
    }

    public void SetSfxVol(float value)
    {
        AudioManager.instance.SetVolume(value, AudioManager.AudioChannel.Sfx);
        PlayerPrefs.SetFloat("sfx vol", value);
        PlayerPrefs.Save();
    }
}
