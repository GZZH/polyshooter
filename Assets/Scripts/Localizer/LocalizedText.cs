﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{
    public string section;
    public string key;

    public Text outputText;

    private void Start()
    {
        Localizer.instance.ChangeLanguage += UpdateText;
        UpdateText();
    }

    public void UpdateText()
    {
        print("????");
        outputText.text = Localizer.instance.GetString(section, key);
    }
}
