﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Localizer : MonoBehaviour
{
    public static Localizer instance;

    public event System.Action ChangeLanguage;

    public string[] languageOptions;

    public string languageOptionsFile;

    string[] localizedLanguageOptions;

    INIParser languageIni;
    INIParser mainIni;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);

            localizedLanguageOptions = new string[languageOptions.Length];

            languageIni = new INIParser();
            mainIni = new INIParser();

            string path = Application.streamingAssetsPath + "/" + languageOptionsFile;
            languageIni.Open(path);

            for(int idx = 0; idx < languageOptions.Length; idx ++)
            {
                localizedLanguageOptions[idx] = GetString("Language", languageOptions[idx], languageIni);
            }
            languageIni.Close();
        }
    }
    
    //void Start ()
    //{
        //UpdateLanguage(1);
    //}
	
	public string[] GetLocalizedLanguageOptions ()
    {
        return localizedLanguageOptions;
    }

    public void UpdateLanguage(int languageIdx)
    {
        mainIni.Close();

        string path = Application.streamingAssetsPath + "/" + languageOptions[languageIdx] + ".ini";
        mainIni.Open(path);

        //print(GetString("TEST", "TEST"));

        ChangeLanguage();
    }

    public string GetString(string section, string key, INIParser ini = null)
    {
        if(ini == null)
        {
            ini = mainIni;
        }
        return ini.ReadValue(section, key, "<Missing_" + key + ">");
    }
}
