﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public event System.Action<int> OnNewWave;
    public event System.Action EnemyCleaned;

    public int waveNum;
    public bool DevMode;

    Player player;
    Spawner spawner;
    MapGenerator2 map;
    GameUI gameUI;
    Mainmap mainmap;
    Vector3 playerSpawnPos;

    int enemiesRemaingAlive;
    bool isInfiniteWave;

    void Start ()
    {
        gameUI = FindObjectOfType<GameUI>();
        spawner = FindObjectOfType<Spawner>();
        map = FindObjectOfType<MapGenerator2>();
        player = FindObjectOfType<Player>();
        player.OnDeath += OnPlayerDeath;
        mainmap = FindObjectOfType<Mainmap>();

        NextWave();
    }
	
	void Update ()
    {
        List<Enemy> enemys = spawner.GetNewEnemys();
        foreach (Enemy enemy in enemys)
        {
            enemy.OnDeath += OnEnemyDeath;
        }

        if(DevMode && !player.IsInvincible())
        {
            player.SetInvincible(true, 5f);
        }

        if (DevMode && Input.GetKeyDown(KeyCode.Return))
        {
            NextWave();
        }

        gameUI.SwitchMap(Input.GetAxisRaw("Map") == 1);
    }

    public void SetPlayerSpawnPos(Vector3 pos)
    {
        playerSpawnPos = pos;
    }

    void OnPlayerDeath()
    {
        spawner.StopSpawning();
    }

    void OnEnemyDeath()
    {
        enemiesRemaingAlive--;
        if (enemiesRemaingAlive <= 0 && !isInfiniteWave)
        {
            //NextWave();
            EnemyCleaned();
        }
    }

    void ResetPlayerPos()
    {
        //Vector2 coord = map.GetRanOpenCoord(new Vector2(1, 1), 1);
        player.transform.position = playerSpawnPos + Vector3.up * 0.5f;
        //map.ReturnCoord(coord, Vector2.zero);
    }

    public void NextWave()
    {
        waveNum++;

        if (OnNewWave != null)
        {
            map.OnNewWave(waveNum);
            OnNewWave(waveNum);
            isInfiniteWave = spawner.IsInfiniteWave();
            enemiesRemaingAlive = spawner.GetTotalEnemy();

            ResetPlayerPos();
            player.SetInvincible(true, 3f);
            AstarPath.active.Scan();
            mainmap.ResetFog();
        }
    }
}
