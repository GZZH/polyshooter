﻿using System.Collections;

public static class Utility
{
    public static T[] ShuffleArray<T>(T[] array, int seed)
    {
        System.Random ranNumGen = new System.Random(seed);
        for (int i = 0; i < array.Length-1; i++)
        {
            int randomIdx = ranNumGen.Next(i, array.Length);
            T tempItem = array[randomIdx];
            array[randomIdx] = array[i];
            array[i] = tempItem;
        }
        return array;
    }
}
