﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public enum AudioChannel { Master, Sfx, Music};

    AudioSource[] musicSources;

    AudioSource sfx2DSource;
    Transform audioListener;
    Transform player;
    SoundLibrary library;

    int activeMusicIdx;
    float masterVolumePercent = 1f;
    float sfxVolumePercent = 1f;
    float musicVolumePercent = 1f;

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        player = null;
        Player tampPlayer = FindObjectOfType<Player>();
        if (tampPlayer != null)
        {
            player = tampPlayer.transform;
        }
    }

    void Awake ()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
            instance = this;
            DontDestroyOnLoad(gameObject);

            library = GetComponent<SoundLibrary>();

            musicSources = new AudioSource[2];
            for (int i = 0; i < musicSources.Length; i++)
            {
                GameObject newMusicSource = new GameObject("MusicSource" + (i + 1));
                musicSources[i] = newMusicSource.AddComponent<AudioSource>();
                newMusicSource.transform.parent = transform;
                musicSources[i].loop = true;
            }

            GameObject newsfx2DSource = new GameObject("2D sfx Source");
            sfx2DSource = newsfx2DSource.AddComponent<AudioSource>();
            sfx2DSource.transform.parent = transform;

            audioListener = FindObjectOfType<AudioListener>().transform;
        }
    }

    private void Update()
    {
        if (player != null)
        {
            audioListener.position = player.position;
        }
    }

    public void SetVolume(float volumePercent, AudioChannel channel)
    {
        switch(channel)
        {
            case (AudioChannel.Master):
                masterVolumePercent = volumePercent;
                break;
            case (AudioChannel.Sfx):
                sfxVolumePercent = volumePercent;
                break;
            case (AudioChannel.Music):
                musicVolumePercent = volumePercent;
                break;
        }
        for (int i = 0; i < musicSources.Length; i++)
        {
            musicSources[i].volume = masterVolumePercent * musicVolumePercent;
        }
    }

    public void PlayMusic(AudioClip clip, float fadeDuration = 1f)
    {

        activeMusicIdx = 1 - activeMusicIdx;
        musicSources[activeMusicIdx].clip = clip;
        musicSources[activeMusicIdx].Play();

        StartCoroutine(MusicCrossfade(fadeDuration));
    }
	public void PlaySound (AudioClip clip, Vector3 pos)
    {
        if (clip != null)
        {
            AudioSource.PlayClipAtPoint(clip, pos, sfxVolumePercent * masterVolumePercent);
        }
    }

    public void PlaySound(string soundName, Vector3 pos)
    {
        PlaySound(library.GetClipByName(soundName), pos);
    }

    public void PlaySound2D(string soundName)
    {
        AudioClip clip = library.GetClipByName(soundName);
        if (clip != null)
        {
            sfx2DSource.PlayOneShot(clip, sfxVolumePercent * masterVolumePercent);
        }
    }

    IEnumerator MusicCrossfade(float duration)
    {
        float percent = 0f;
        float speed = 1f / duration;
        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            musicSources[activeMusicIdx].volume = Mathf.Lerp(0, musicVolumePercent * masterVolumePercent, percent);
            musicSources[1 - activeMusicIdx].volume = Mathf.Lerp(musicVolumePercent * masterVolumePercent, 0, percent);
            yield return null;
        }
    }
}
