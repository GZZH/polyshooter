﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    public AudioClip mainMusic;
    public AudioClip menuMusic;

    string sceneName;


    void Start ()
    {
        OnLevelWasLoaded(0);
    }

    void OnLevelWasLoaded(int level)
    {
        string newSceneName = SceneManager.GetActiveScene().name;
        if(sceneName != newSceneName)
        {
            sceneName = newSceneName;
            //Invoke("PlayMusic", 0.1f);
        }

    }

    void PlayMusic()
    {
        AudioClip clip = null;
        if(sceneName == "Menu")
        {
            clip = menuMusic;
        }
        else if (sceneName == "Main")
        {
            clip = mainMusic;
        }

        if(clip != null)
        {
            AudioManager.instance.PlayMusic(clip, 2f);
        }
    }
}
