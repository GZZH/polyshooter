﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoBox : MapElement
{
    public AudioClip pickUpAudio;

    public float ammoAmount = 0.1f;

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent<GunController>().AddAmmo(ammoAmount))
        {
            AudioManager.instance.PlaySound(pickUpAudio, transform.position);
            Destroy(gameObject);
        }
    }
}
