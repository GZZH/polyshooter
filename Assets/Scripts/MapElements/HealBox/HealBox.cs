﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealBox : MapElement
{
    public AudioClip pickUpAudio;

    public float healAmount = 1f;

    protected override void OnTriggerEnter(Collider other)
    {
        if (other.GetComponentInParent<Player>().AddHealth(healAmount))
        {
            AudioManager.instance.PlaySound(pickUpAudio, transform.position);
            Destroy(gameObject);
        }
    }
}
