﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Elevator : MapElement
{
    public LayerMask triggerLayer;
    public Transform spawnPos;
    public Transform leftGate;
    public Transform rightGate;
    public Transform lightBulb;
    public Light lightSingal;
    public Color gateOpenLightColor;
    public Color gateCloseLightColor;

    public AudioClip doorOpenAudio;
    public AudioClip doorCloseAudio;

    public float gateOpenPos;
    public float gateVelocity;

    Material lightBulbMat;
    Collider gateCollider;
    MapGenerator2 map;

    bool isGateMoving;
    bool gateOpen;
    bool lightColor;
    bool isStartElevator;
    bool isExit;

    void Start ()
    {
        map = FindObjectOfType<MapGenerator2>();
        lightBulbMat = lightBulb.GetComponent<MeshRenderer>().material;
        //MoveFloorTo(2.9f);
        FindObjectOfType<GameManager>().EnemyCleaned += EnemyCleaned;
        gateCollider = leftGate.parent.GetComponent<BoxCollider>();
        //ControlGate(true);
    }

    public void SetExit(bool state)
    {
        isExit = state;
        if (!isExit)
        {
            FindObjectOfType<GameManager>().SetPlayerSpawnPos(spawnPos.position);
            ControlGate(true);
        }
    }

    void EnemyCleaned()
    {
        if(isExit)
        {
            ControlGate(true);
        }
    }

    protected override void OnTriggerEnter(Collider collider)
    {
        if (triggerLayer == (triggerLayer | (1 << collider.gameObject.layer)))
        {
            //MoveFloorTo(0f);
            ControlGate(true);
        }
    }

    protected override void OnTriggerExit(Collider collider)
    {
        if (triggerLayer == (triggerLayer | (1 << collider.gameObject.layer)))
        {
            ControlGate(false);
            if(isExit)
            {
                FindObjectOfType<GameManager>().NextWave();
            }
        }
    }

    void Update ()
    {
		if(lightColor != gateOpen)
        {
            lightBulbMat.color = gateOpen ? gateOpenLightColor : gateCloseLightColor;
            lightSingal.color = gateOpen ? gateOpenLightColor : gateCloseLightColor;
            lightColor = gateOpen;
        }
    }
    
    public Vector3 GetSpawnPos()
    {
        return spawnPos.position;
    }

    public void IsStartElevator()
    {
        //floor.localPosition = new Vector3(floor.localPosition.x, startFloorHeight, floor.localPosition.z);
        leftGate.localPosition = new Vector3(leftGate.localPosition.x, leftGate.localPosition.y, -gateOpenPos);
        rightGate.localPosition = new Vector3(leftGate.localPosition.x, leftGate.localPosition.y, gateOpenPos);
    }

    public void ControlGate(bool state)
    {
        if(state && !gateOpen)
        {
            StartCoroutine(AnimateOpenGate());
        }
        else if(!state && gateOpen)
        {
            StartCoroutine(AnimateCloseGate());
        }
    }

    IEnumerator AnimateOpenGate()
    {
        if (!gateOpen)
        {
            AudioManager.instance.PlaySound(doorOpenAudio, transform.position);
            float percent = 0;
            isGateMoving = true;
            while (percent < 1 && !gateOpen)
            {
                percent += Time.deltaTime * gateVelocity;
                float gatePos = Mathf.Lerp(0.03f, gateOpenPos, percent);
                leftGate.localPosition = new Vector3(leftGate.localPosition.x, leftGate.localPosition.y, -gatePos);
                rightGate.localPosition = new Vector3(leftGate.localPosition.x, leftGate.localPosition.y, gatePos);
                yield return null;
            }
            gateOpen = true;
            isGateMoving = false;
            gateCollider.enabled = false;
        }
    }

    IEnumerator AnimateCloseGate()
    {
        if(gateOpen)
        {
            AudioManager.instance.PlaySound(doorCloseAudio, transform.position);
            float percent = 0;
            isGateMoving = true;
            while (percent < 1)
            {
                percent += Time.deltaTime * gateVelocity;
                float gatePos = Mathf.Lerp(gateOpenPos, 0.03f, percent);
                leftGate.localPosition = new Vector3(leftGate.localPosition.x, leftGate.localPosition.y, -gatePos);
                rightGate.localPosition = new Vector3(leftGate.localPosition.x, leftGate.localPosition.y, gatePos);
                yield return null;
            }
            gateOpen = false;
            isGateMoving = false;
            gateCollider.enabled = true;
        }
    }
}
