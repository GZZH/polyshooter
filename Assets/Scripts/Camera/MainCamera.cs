﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour 
{
    public Player player;
    public Transform crossHair;

    public float smoothTime;

    Vector3 targetPos;
    Vector3 cameraSmoothVelocity;

    float viewRange;
    float offsetPos;


    void Start ()
    {
        offsetPos = transform.position.z;
        viewRange = player.GetViewRange()*0.75f;
    }
	
	void FixedUpdate ()
    {
        viewRange = player.GetViewRange()*0.75f;
        if (player != null)
        {
            targetPos = new Vector3(((crossHair.position.x - player.transform.position.x)/ 3f) + player.transform.position.x,
                                    viewRange, 
                                    offsetPos + ((crossHair.position.z - player.transform.position.z) / 3f) + player.transform.position.z);
        }
        transform.position = Vector3.SmoothDamp(transform.position, targetPos,ref cameraSmoothVelocity, smoothTime);
    }
}
