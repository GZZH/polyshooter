﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator2 : MonoBehaviour
{
    public Map[] maps;

    public Transform pointer;

    public int mapIdx;
    public int smoothNum;
    public float squareSize = 1f;

    List<Room> rooms;
    List<Coord> usingCoords;

    Map currentMap;
    System.Random numGenerator;


    public void Awake()
    {
        GenerateMap();
    }

    public void OnNewWave(int waveNum)
    {
        if (waveNum <= maps.Length)
        {
            mapIdx = waveNum - 1;
        }
        else
        {
            currentMap.useRandomSeed = true;
        }
        GenerateMap();
    }

    public void GenerateMap()
    {
        currentMap = maps[mapIdx];

        numGenerator = new System.Random(currentMap.seed);
        usingCoords = new List<Coord>();
        currentMap.map = new int[currentMap.mapSize.x + currentMap.borderSize * 2, currentMap.mapSize.y + currentMap.borderSize * 2];

        RandomFillMap();

        for (int idx = 0; idx < smoothNum; idx++)
        {
            SmoothMap();
        }

        rooms = new List<Room>();
        ProcessMap();

        MeshGenerator meshGen = GetComponent<MeshGenerator>();
        meshGen.GenerateMesh(currentMap.map, squareSize, currentMap.wallHeight, currentMap.foregroundColor, currentMap.backgroundColor);

        CoordToWorld(new Coord(15, 15));
    }

    void ProcessMap()
    {
        List<List<Coord>> wallRegions = GetRegions(1);

        foreach(List<Coord> wallRegion in wallRegions)
        {
            if(wallRegion.Count < currentMap.wallThresholdSize)
            {
                foreach(Coord coord in wallRegion)
                {
                    currentMap.map[coord.x, coord.y] = 0;
                }
            }
        }

        rooms.Clear();
        List<List<Coord>> roomRegions = GetRegions(0);

        List<Coord> tempReigon = new List<Coord>();
        foreach (List<Coord> roomRegion in roomRegions)
        {
            if (roomRegion.Count < currentMap.roomThresholdSize)
            {
                if(tempReigon.Count < roomRegion.Count)
                {
                    tempReigon = roomRegion;
                    foreach (Coord coord in tempReigon)
                    {
                        currentMap.map[coord.x, coord.y] = 1;
                    }
                }
                else
                {
                    foreach (Coord coord in roomRegion)
                    {
                        currentMap.map[coord.x, coord.y] = 1;
                    }
                }
            }
            else
            {
                rooms.Add(new Room(roomRegion, currentMap.map, currentMap.seed));
            }
        }
        if (rooms.Count == 0)
        {
            rooms.Add(new Room(tempReigon, currentMap.map, currentMap.seed));
        }
        else
        {
            foreach (Coord coord in tempReigon)
            {
                currentMap.map[coord.x, coord.y] = 1;
            }
        }

        rooms.Sort();
        rooms[0].IsMain = true;
        rooms[0].IsConnectedMain = true;
        ConnectClosestRooms();
    }

    void ConnectClosestRooms(bool connectingMain = false)
    {
        List<Room> mainConnectedRooms = new List<Room>();
        List<Room> mainNotConnectedRooms = new List<Room>();

        if(connectingMain)
        {
            foreach(Room room in rooms)
            {
                if (room.IsConnectedMain)
                {
                    mainConnectedRooms.Add(room);
                }
                else
                {
                    mainNotConnectedRooms.Add(room);
                }
            }
        }
        else
        {
            mainConnectedRooms = rooms;
            mainNotConnectedRooms = rooms;
        }
            
        int bestDistance = 0;
        Room bestRoomA = new Room();
        Room bestRoomB = new Room();
        Coord bestTileA = new Coord();
        Coord bestTileB = new Coord();

        foreach (Room roomA in mainNotConnectedRooms)
        {
            if(!connectingMain)
            {
                bestDistance = 0;
            }
            foreach (Room roomB in mainConnectedRooms)
            {
                if (roomA != roomB)
                {

                    for (int idxA = 0; idxA < roomA.edgeTiles.Count; idxA++)
                    {
                        Coord tileA = roomA.edgeTiles[idxA];
                        for (int idxB = 0; idxB < roomB.edgeTiles.Count; idxB++)
                        {
                            Coord tileB = roomB.edgeTiles[idxB];
                            int distance = (int)(Mathf.Pow(tileA.x - tileB.x, 2) + Mathf.Pow(tileA.y - tileB.y, 2));
                            if (bestDistance == 0 || distance < bestDistance)
                            {
                                bestDistance = distance;
                                bestRoomA = roomA;
                                bestRoomB = roomB;
                                bestTileA = tileA;
                                bestTileB = tileB;
                            }
                        }
                    }
                }
            }
            if (bestDistance != 0 && !connectingMain && !bestRoomA.IsConnected(bestRoomB))
            {
                CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }
        if (bestDistance != 0 && connectingMain && !bestRoomA.IsConnected(bestRoomB))
        {
            CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            ConnectClosestRooms(true);
        }
        if (!connectingMain)
        {
            ConnectClosestRooms(true);
        }
    }

    void CreatePassage(Room roomA, Room roomB, Coord tileA, Coord tileB)
    {
        Room.ConnectRooms(roomA,roomB);

        List<Coord> line = GetLine(tileA, tileB);
        foreach(Coord coord in line)
        {
            DrawCircle(coord, currentMap.passageR);
        }
    }

    void DrawCircle(Coord coord, int r)
    {
        for(int x = -r; x <= r; x ++)
        {
            for (int y = -r; y <= r; y++)
            {
                if (x * x + y * y <= r * r)
                {
                    int drawX = coord.x + x;
                    int drawY = coord.y + y;
                    if(currentMap.IsInMap(drawX,drawY))
                    {
                        currentMap.map[drawX, drawY] = 0;
                    }
                }
            }
        }
    }

    List<Coord> GetLine(Coord from, Coord to)
    {
        List<Coord> line = new List<Coord>();

        int x = from.x;
        int y = from.y;

        int dx = to.x - from.x;
        int dy = to.y - from.y;

        bool inverted = false;

        int step = Math.Sign(dx);
        int gradientStep = Math.Sign(dy);

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dy);

        if(longest < shortest)
        {
            inverted = true;
            shortest = Mathf.Abs(dx);
            longest = Mathf.Abs(dy);

            gradientStep = Math.Sign(dx);
            step = Math.Sign(dy);
        }

        int gradientAccumulation = longest / 2;
        for(int idx = 0; idx < longest; idx++)
        {
            line.Add(new Coord(x, y));

            if(inverted)
            {
                y += step;
            }
            else
            {
                x += step;
            }

            gradientAccumulation += shortest;
            if(gradientAccumulation >= longest)
            {
                if(inverted)
                {
                    x += gradientStep;
                }
                else
                {
                    y += gradientStep;
                }
                gradientAccumulation -= longest;
            }
        }
        return line;
    }

    public Vector3 CoordToWorld(Coord coord)
    {
        pointer.localPosition = new Vector3(coord.x * squareSize - currentMap.map.GetLength(0) * squareSize / 2f + squareSize / 2f, 
                                            0, 
                                            coord.y * squareSize - currentMap.map.GetLength(1) * squareSize / 2f + squareSize / 2f);
        return pointer.position;
    }

    public Vector3 CoordToWorld(Vector2 coord)
    {
        return (CoordToWorld(new Coord((int)coord.x, (int)coord.y)));
    }

    public Vector2 WorldToCoord(Vector3 pos)
    {
        pointer.position = pos;
        Vector2 pointCoord = new Vector2((int)((pointer.localPosition.x - squareSize / 2 + currentMap.map.GetLength(0) * squareSize / 2f) / squareSize), 
                                         (int)((pointer.localPosition.z - squareSize / 2 + currentMap.map.GetLength(1) * squareSize / 2f) / squareSize));
        return pointCoord;
    }

    List<List<Coord>> GetRegions(int tileType)
    {
        List<List<Coord>> regions = new List<List<Coord>>();
        int[,] mapFlags = new int[currentMap.map.GetLength(0), currentMap.map.GetLength(1)];
        for (int x = 0; x < currentMap.map.GetLength(0); x++)
        {
            for (int y = 0; y < currentMap.map.GetLength(1); y++)
            {
                if(mapFlags[x,y] == 0 && currentMap.map[x,y] == tileType)
                {
                    List<Coord> reigon = GetRegionTiles(x, y);
                    regions.Add(reigon);
                    foreach(Coord coord in reigon)
                    {
                        mapFlags[coord.x, coord.y] = 1;
                    }
                }
            }
        }
        return regions;
    }

    List<Coord> GetRegionTiles(int startX, int startY)
    {
        List<Coord> tiles = new List<Coord>();
        int[,] mapFlags = new int[currentMap.map.GetLength(0), currentMap.map.GetLength(1)];
        int tileType = currentMap.map[startX, startY];

        Queue<Coord> queue = new Queue<Coord>();
        queue.Enqueue(new Coord(startX, startY));

        mapFlags[startX, startY] = 1;

        while(queue.Count > 0)
        {
            Coord tile = queue.Dequeue();
            tiles.Add(tile);

            for (int x = tile.x - 1; x <= tile.x + 1; x++)
            {
                for (int y = tile.y - 1; y <= tile.y + 1; y++)
                {
                    if (currentMap.IsInMap(x, y) && (x == tile.x || y == tile.y) && mapFlags[x, y] == 0 && currentMap.map[x, y] == tileType)
                    {
                        mapFlags[x, y] = 1;
                        queue.Enqueue(new Coord(x, y));
                    }
                }
            }
        }

        return tiles;
    }

    public bool IsCoordOpen(Vector2 coord)
    {
        return currentMap.IsInMap((int)coord.x, (int)coord.y) && currentMap.map[(int)coord.x, (int)coord.y] == 0 &&!usingCoords.Contains(new Coord((int)coord.x, (int)coord.y));
    }

    public bool IsCoordOpen(Coord coord)
    {
        return currentMap.IsInMap(coord.x, coord.y) && currentMap.map[coord.x, coord.y] == 0 && !usingCoords.Contains(coord);
    }

    public Vector2 GetRanOpenCoord(Vector2 size, int roomIdx = -1)
    {
        Coord returning = new Coord(0,0);
        bool passTest = false;
        while (!passTest)
        {
            if (roomIdx >= 0 && roomIdx < rooms.Count)
            {
                returning = rooms[roomIdx].GetRanCoord();
            }
            else
            {
                returning = rooms[numGenerator.Next(0, rooms.Count)].GetRanCoord();
            }
            passTest = true;
            for (int x = (int)(-size.x / 2); x <= (int)(size.x / 2); x++)
            {
                for (int y = (int)(-size.y / 2); y <= (int)(size.y / 2); y++)
                {
                    int testX = returning.x + x;
                    int testY = returning.y + y;

                    if(passTest)
                    {
                        if (!IsCoordOpen(new Coord(testX, testY)))
                        {
                            passTest = false;
                        }
                    }
                }
            }
        }
        UseCoord(new Vector2(returning.x, returning.y), size);
        return new Vector2(returning.x, returning.y);
    }

    public void UseCoord(Vector2 _coord, Vector2 size)
    {
        for (int x = (int)(-size.x / 2); x <= (int)(size.x / 2); x++)
        {
            for (int y = (int)(-size.y / 2); y <= (int)(size.y / 2); y++)
            {
                Coord coord = new Coord((int)_coord.x + x, (int)_coord.y + y);
                usingCoords.Add(coord);
            }
        }
    }

    public void ReturnCoord(Vector2 _coord, Vector2 size)
    {
        for (int x = (int)(-size.x / 2); x <= (int)(size.x / 2); x++)
        {
            for (int y = (int)(-size.y / 2); y <= (int)(size.y / 2); y++)
            {
                Coord coord = new Coord((int)_coord.x + x, (int)_coord.y + y);
                if (usingCoords.Contains(coord))
                {
                    usingCoords.Remove(coord);
                }
            }
        }
    }

    void RandomFillMap()
    {
        if (currentMap.useRandomSeed)
        {
            currentMap.seed = UnityEngine.Random.Range(0, Mathf.Abs(DateTime.Now.GetHashCode()));
        }

        for (int x = 0; x < currentMap.map.GetLength(0); x++)
        {
            for (int y = 0; y < currentMap.map.GetLength(1); y++)
            {
                if (x >= currentMap.borderSize && x < currentMap.mapSize.x + currentMap.borderSize && y >= currentMap.borderSize && y < currentMap.mapSize.y + currentMap.borderSize)
                {
                    currentMap.map[x, y] = (numGenerator.Next(0, 100) < currentMap.wallPrecent) ? 1 : 0;
                }
                else
                {
                    currentMap.map[x, y] = 1;
                }
            }
        }
    }

    void SmoothMap()
    {
        for (int x = 0; x < currentMap.map.GetLength(0); x++)
        {
            for (int y = 0; y < currentMap.map.GetLength(1); y++)
            {
                int neighbourWallCount = GetNextWallCount(x, y);
                if (neighbourWallCount > 4)
                {
                    currentMap.map[x, y] = 1;
                }else if(neighbourWallCount < 4)
                {
                    currentMap.map[x, y] = 0;
                }
            }
        }
    }

    int GetNextWallCount(int xpos, int ypos)
    {
        int wallCount = 0;
        for (int x = xpos - 1; x <= xpos + 1; x++)
        {
            for (int y = ypos - 1; y <= ypos + 1; y++)
            {
                if (currentMap.IsInMap(x, y))
                {
                    if (x != xpos || y != ypos)
                    {
                        wallCount += currentMap.map[x, y];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }
        return wallCount;
    }

    class Room : IComparable<Room>
    {
        public List<Coord> tiles;
        public List<Coord> edgeTiles;
        public List<Room> connectedRooms;
        public Queue<Coord> shuffledopenCoords;
        public int roomSize;
        public bool IsMain;
        public bool IsConnectedMain;

        public Room()
        {

        }

        public Room(List<Coord> _tiles, int[,] map, int seed)
        {
            tiles = _tiles;
            roomSize = tiles.Count;

            edgeTiles = new List<Coord>();
            connectedRooms = new List<Room>();
            
            foreach (Coord tile in tiles)
            {
                for (int x = tile.x - 1; x <= tile.x + 1; x++)
                {
                    for (int y = tile.y - 1; y <= tile.y + 1; y++)
                    {
                        if ((x == tile.x || y == tile.y) && map[x, y] == 1)
                        {
                            edgeTiles.Add(tile);
                        }
                    }
                }
            }
            
            shuffledopenCoords = new Queue<Coord>(Utility.ShuffleArray(tiles.ToArray(), seed));
        }

        public void ConnectToMain()
        {
            if(!IsConnectedMain)
            {
                IsConnectedMain = true;
                foreach(Room room in connectedRooms)
                {
                    room.ConnectToMain();
                }
            }
        }

        public static void ConnectRooms(Room roomA, Room roomB)
        {
            if(roomA.IsConnectedMain)
            {
                roomB.ConnectToMain();
            }else if (roomB.IsConnectedMain)
            {
                roomA.ConnectToMain();
            }
            roomA.connectedRooms.Add(roomB);
            roomB.connectedRooms.Add(roomA);
        }

        public bool IsConnected(Room room)
        {
            return connectedRooms.Contains(room);
        }

        public int CompareTo(Room otherRoom)
        {
            return otherRoom.roomSize.CompareTo(roomSize);
        }

        public Coord GetRanCoord()
        {
            Coord randomCoord = shuffledopenCoords.Dequeue();
            shuffledopenCoords.Enqueue(randomCoord);
            return randomCoord;
        }
    }

    [Serializable]
    public struct Coord
    {
        public int x;
        public int y;

        public Coord(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public static bool operator ==(Coord c1, Coord c2f)
        {
            return c1.x == c2f.x && c1.y == c2f.y;
        }

        public static bool operator !=(Coord c1, Coord c2f)
        {
            return c1.x != c2f.x || c1.y != c2f.y;
        }
    }

    [Serializable]
    public class Map
    {
        public Coord mapSize;
        public Color foregroundColor;
        public Color backgroundColor;

        [Range(0, 100)]
        public float wallPrecent;
        public float wallHeight = 5f;

        public int wallThresholdSize;
        public int roomThresholdSize;
        public int borderSize = 5;
        public int passageR = 1;

        public int seed;
        public bool useRandomSeed;

        public int[,] map;

        public bool IsInMap(int x, int y)
        {
            return x >= 0 && x < map.GetLength(0) && y >= 0 && y < map.GetLength(1);
        }
    }
}
