﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Wave[] waves;

    public GameObject warning;

    [Range(0, 3)]
    public int enemyLevel;

    List<Enemy> newEnemys;

    MapGenerator2 map;
    Wave currentWave;
    Transform enemyHolder;
    Transform mapElementsHolder;
    LivingEntity playerEntity;
    Transform player;
    Vector3 playerLastPos;

    int currentWaveNum;
    int enemiesRemaingToSpawn;
    float playerPosCheckTime = 2f;//s
    float leastMoveDistance = 1.5f;
    float nextSpawnTime;
    float nextPosCheckTime;
    bool isMoving = true;
    bool isSpawning = false;
    string enemysHolderName = "Enemy Holder";
    string mapElementsHolderName = "Map Elements Holder";

    void Awake()
    {
        FindObjectOfType<GameManager>().OnNewWave += OnNewWave;

        map = FindObjectOfType<MapGenerator2>();
        playerEntity = FindObjectOfType<Player>();
        player = playerEntity.transform;

        newEnemys = new List<Enemy>();
    }

    public void StartSpawning()
    {
        isSpawning = true;
    }

    public void StopSpawning()
    {
        isSpawning = false;
        StopCoroutine("SpawmEnemy");
    }

    public bool IsInfiniteWave()
    {
        return currentWave.infinite;
    }

    public int GetTotalEnemy()
    {
        return currentWave.enemyCount;
    }

    void Update()
    {
        if (Time.time > nextPosCheckTime && isSpawning)
        {
            nextPosCheckTime = Time.time + playerPosCheckTime;
            isMoving = (Vector3.Distance(player.position, playerLastPos) > leastMoveDistance);
            playerLastPos = player.position;
        }

        if ((enemiesRemaingToSpawn > 0 || currentWave.infinite) && Time.time > nextSpawnTime && isSpawning)
        {
            enemiesRemaingToSpawn--;

            StartCoroutine("SpawnEnemy");
            nextSpawnTime = Time.time + currentWave.spawnsCD;
        }
    }

    public List<Enemy> GetNewEnemys()
    {
        List<Enemy> tempEnemys = new List<Enemy>();
        foreach(Enemy enemy in newEnemys)
        {
            tempEnemys.Add(enemy);
        }
        newEnemys.Clear();
        return tempEnemys;
    }

    IEnumerator SpawnEnemy()
    {
        float spawnTimer = 0f;
        float spawnDelay = 1f;
        Vector3 spawnPos;
        Vector2 usingCoord;

        if(!isMoving)
        {
            isMoving = true;
            spawnPos = playerLastPos;
            usingCoord = map.WorldToCoord(spawnPos);
            if (!map.IsCoordOpen(usingCoord))
            {
                usingCoord = map.GetRanOpenCoord(Vector2.zero);
            }
            else
            {
                map.UseCoord(usingCoord, Vector2.zero);
            }
        }
        else
        {
            usingCoord = map.GetRanOpenCoord(Vector2.zero);
        }

        spawnPos = map.CoordToWorld(usingCoord);

        Enemy newEnemy = Instantiate(currentWave.enemys[0], spawnPos + Vector3.down * 5, Quaternion.identity) as Enemy;
        newEnemy.SetLevel(enemyLevel, currentWave.skinColor);
        newEnemy.transform.SetParent(enemyHolder);
        newEnemys.Add(newEnemy);
        GameObject warningSign = Instantiate(warning, spawnPos, Quaternion.identity);
        yield return new WaitForSecondsRealtime(spawnDelay);
        while (spawnTimer < spawnDelay)
        {
            newEnemy.transform.position = Vector3.Lerp(spawnPos + Vector3.down * 2, spawnPos + Vector3.up, spawnTimer / spawnDelay);

            spawnTimer += Time.deltaTime;
            yield return null;
        }
        Destroy(warningSign);

        newEnemy.transform.position = spawnPos + Vector3.up;
        newEnemy.Active();

        map.ReturnCoord(usingCoord, Vector2.zero);
    }

    void SpawnMapElements()
    {
        Vector3 spawnPos;
        Vector2 usingCoord;
        bool hasEntrance = false;

        for (int idx = 0; idx < 2; idx++)
        {
            usingCoord = map.GetRanOpenCoord(currentWave.elevator.GetSize());
            spawnPos = map.CoordToWorld(usingCoord);

            Elevator newElevator = Instantiate(currentWave.elevator, spawnPos, Quaternion.Euler(0, Random.Range(135, 45), 0)) as Elevator;
            newElevator.transform.SetParent(mapElementsHolder);
            newElevator.SetExit(hasEntrance);
            hasEntrance = true;
        }

        foreach (ElementsInWave currentElement in currentWave.elements)
        {
            for (int idx = 0; idx < currentElement.emlmentNum; idx++)
            {
                usingCoord = map.GetRanOpenCoord(currentElement.element.GetSize());
                spawnPos = map.CoordToWorld(usingCoord);

                MapElement newElement = Instantiate(currentElement.element, spawnPos, Quaternion.Euler(0, Random.Range(135, 45), 0));
                newElement.transform.SetParent(mapElementsHolder);
            }
        }
    }
    
    void OnNewWave(int waveNum)
    {
        StopSpawning();

        newEnemys.Clear();
        if (transform.Find(enemysHolderName))
        {
            Destroy(transform.Find(enemysHolderName).gameObject);
        }
        enemyHolder = new GameObject(enemysHolderName).transform;
        enemyHolder.SetParent(transform);
        
        if (transform.Find(mapElementsHolderName))
        {
            Destroy(transform.Find(mapElementsHolderName).gameObject);
        }
        mapElementsHolder = new GameObject(mapElementsHolderName).transform;
        mapElementsHolder.SetParent(transform);

        currentWaveNum = waveNum - 1;

        if (currentWaveNum < waves.Length)
        {
            currentWave = waves[currentWaveNum];

            enemiesRemaingToSpawn = currentWave.enemyCount;

            playerLastPos = player.position;
            nextPosCheckTime = Time.time + playerPosCheckTime;

            StartSpawning();
            SpawnMapElements();
        }
    }

    [System.Serializable]
    public class Wave
    {
        [Header("----------Enemys----------")]
        public Enemy[] enemys;

        public Color skinColor;

        public int enemyCount;
        public float spawnsCD;//s
        public bool infinite;

        [Header("----------MapElements----------")]
        public Elevator elevator;
        public ElementsInWave[] elements;
    }

    [System.Serializable]
    public class ElementsInWave
    {
        public MapElement element;
        public int emlmentNum;
    }
}
