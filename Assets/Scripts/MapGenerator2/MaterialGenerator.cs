﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialGenerator : MonoBehaviour
{
    Color foregroundColor;
    Color backgroundColor;

    int resolutionPerSquare = 2;

    public Texture2D GetTexture(Color foregroundColor, Color backgroundColor, int[,] map)
    {
        Texture2D texture = new Texture2D(map.GetLength(0) * resolutionPerSquare, map.GetLength(1) * resolutionPerSquare);
        Color[] colormap = new Color[map.GetLength(0) * resolutionPerSquare * map.GetLength(1) * resolutionPerSquare];

        texture.filterMode = FilterMode.Point;
        texture.wrapMode = TextureWrapMode.Clamp;
        for (int idx = 0; idx < colormap.GetLength(0); idx++)
        {
            //print(idx / (float)colormap.GetLength(0));
            colormap[idx] = Color.Lerp(foregroundColor, backgroundColor, idx / (float)colormap.GetLength(0));
        }
        texture.SetPixels(colormap);
        texture.Apply();

        return texture;
    }

    //public Texture2D Get
}
