﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    public SquareGrid squareGrid;
    public MeshFilter obstacleMeshFilter;
    public MeshFilter wallMeshFilter;
    public MeshFilter mapFloorMeshFilter;

    List<Vector3> vertices;
    List<int> triangles;
    List<List<int>> outlines = new List<List<int>>();
    HashSet<int> checkedVertices = new HashSet<int>();
    Dictionary<int, List<Triangle>> triangleDict = new Dictionary<int, List<Triangle>>();

    int[,] map;
    float wallHeight;
    float squareSize;

    public void GenerateMesh(int[,] _map, float _squareSize, float _wallHeight, Color foregroundColor, Color backgroundColor)
    {
        map = _map;
        wallHeight = _wallHeight;
        squareSize = _squareSize;

        triangleDict.Clear();
        outlines.Clear();
        checkedVertices.Clear();

        obstacleMeshFilter.transform.position = new Vector3(obstacleMeshFilter.transform.position.x, wallHeight, obstacleMeshFilter.transform.position.z);
        wallMeshFilter.transform.position = new Vector3(wallMeshFilter.transform.position.x, wallHeight, wallMeshFilter.transform.position.z);
        mapFloorMeshFilter.transform.position = new Vector3(mapFloorMeshFilter.transform.position.x, -1, mapFloorMeshFilter.transform.position.z);

        CreateMapFloorMesh();

        CreateObstacleMesh();

        CreateWallMesh();

        MaterialGenerator matGenerator = transform.GetComponent<MaterialGenerator>();
        obstacleMeshFilter.transform.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = matGenerator.GetTexture(foregroundColor, backgroundColor, map);
    }

    Vector2[] CalUvs(List<Vector3> vertices)
    {
        Vector2[] uvs = new Vector2[vertices.Count];
        for (int i = 0; i < vertices.Count; i++)
        {
            float percentX = Mathf.InverseLerp(-map.GetLength(0) / 2 * squareSize, map.GetLength(0) / 2 * squareSize, vertices[i].x);
            float percentY = Mathf.InverseLerp(-map.GetLength(0) / 2 * squareSize, map.GetLength(0) / 2 * squareSize, vertices[i].z);
            uvs[i] = new Vector2(percentX, percentY);
        }
        return uvs;
    }

    void CreateMapFloorMesh()
    {
        int[,] _map = new int[map.GetLength(0), map.GetLength(1)];
        for (int x = 0; x < map.GetLength(0); x++)
        {
            for (int y = 0; y < map.GetLength(1); y++)
            {
                _map[x, y] = (map[x, y] == 1) ? 0 : 1;
            }
        }
        squareGrid = new SquareGrid(_map, squareSize);

        vertices = new List<Vector3>();
        triangles = new List<int>();

        for (int x = 0; x < squareGrid.squares.GetLength(0); x++)
        {
            for (int y = 0; y < squareGrid.squares.GetLength(1); y++)
            {
                TriangulateSquare(squareGrid.squares[x, y]);
            }
        }

        Mesh mesh = new Mesh();
        mapFloorMeshFilter.mesh = mesh;

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
        triangleDict.Clear();
        outlines.Clear();
        checkedVertices.Clear();
    }

    void CreateObstacleMesh()
    {
        squareGrid = new SquareGrid(map, squareSize);

        vertices = new List<Vector3>();
        triangles = new List<int>();

        for (int x = 0; x < squareGrid.squares.GetLength(0); x++)
        {
            for (int y = 0; y < squareGrid.squares.GetLength(1); y++)
            {
                TriangulateSquare(squareGrid.squares[x, y]);
            }
        }

        Mesh mesh = new Mesh();
        obstacleMeshFilter.mesh = mesh;

        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
        MeshCollider obstacleCollider = obstacleMeshFilter.gameObject.GetComponent<MeshCollider>();
        obstacleCollider.sharedMesh = mesh;

        //AstarPath.active.UpdateGraphs(obstacleCollider.bounds);
        obstacleMeshFilter.sharedMesh.uv = CalUvs(vertices);
    }

    void CreateWallMesh()
    {
        CalOutlines();

        List<Vector3> wallVertices = new List<Vector3>();
        List<int> wallTriangles = new List<int>();
        Mesh wallMesh = new Mesh();

        foreach(List<int> outline in outlines)
        {
            for(int idx = 0; idx < outline.Count-1; idx++)
            {
                int startIdx = wallVertices.Count;
                wallVertices.Add(vertices[outline[idx]]);//left(outline start point)
                wallVertices.Add(vertices[outline[idx+1]]);//right(outline end point)
                wallVertices.Add(vertices[outline[idx]] - Vector3.up * wallHeight);//bottom left
                wallVertices.Add(vertices[outline[idx + 1]] - Vector3.up * wallHeight);//bottom right

                wallTriangles.Add(startIdx + 0);
                wallTriangles.Add(startIdx + 2);
                wallTriangles.Add(startIdx + 3);

                wallTriangles.Add(startIdx + 3);
                wallTriangles.Add(startIdx + 1);
                wallTriangles.Add(startIdx + 0);
            }
        }
        wallMesh.vertices = wallVertices.ToArray();
        wallMesh.triangles = wallTriangles.ToArray();
        wallMeshFilter.mesh = wallMesh;
        wallMesh.RecalculateNormals();

        MeshCollider wallCollider = wallMeshFilter.gameObject.GetComponent<MeshCollider>();
        wallCollider.sharedMesh = wallMesh;

        //AstarPath.active.UpdateGraphs(wallCollider.bounds);

        wallMeshFilter.sharedMesh.uv = CalUvs(wallVertices);
    }

    void TriangulateSquare(Square square)
    {
        switch(square.configuration)
        {
            // 0 points
            case 0:
                break;

            // 1 points
            case 1:
                MeshFromPoints(square.centerLeft, square.centerBottom, square.bottomLeft);
                break;
            case 2:
                MeshFromPoints(square.bottomRight, square.centerBottom, square.centerRight);
                break;
            case 4:
                MeshFromPoints(square.topRight, square.centerRight, square.centerTop);
                break;
            case 8:
                MeshFromPoints(square.topLeft, square.centerTop, square.centerLeft);
                break;

            // 2 points
            case 3:
                MeshFromPoints(square.centerRight, square.bottomRight, square.bottomLeft, square.centerLeft);
                break;
            case 6:
                MeshFromPoints(square.centerTop, square.topRight, square.bottomRight, square.centerBottom);
                break;
            case 9:
                MeshFromPoints(square.topLeft, square.centerTop, square.centerBottom, square.bottomLeft);
                break;
            case 12:
                MeshFromPoints(square.topLeft, square.topRight, square.centerRight, square.centerLeft);
                break;
            case 5:
                MeshFromPoints(square.centerTop, square.topRight, square.centerRight, square.centerBottom, square.bottomLeft, square.centerLeft);
                break;
            case 10:
                MeshFromPoints(square.topLeft, square.centerTop, square.centerRight, square.bottomRight, square.centerBottom, square.centerLeft);
                break;

            // 3 points
            case 7:
                MeshFromPoints(square.centerTop, square.topRight, square.bottomRight, square.bottomLeft, square.centerLeft);
                break;
            case 11:
                MeshFromPoints(square.topLeft, square.centerTop, square.centerRight, square.bottomRight, square.bottomLeft);
                break;
            case 13:
                MeshFromPoints(square.topLeft, square.topRight, square.centerRight, square.centerBottom, square.bottomLeft);
                break;
            case 14:
                MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.centerBottom, square.centerLeft);
                break;

            // 4 points
            case 15:
                MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.bottomLeft);
                if(!square.isOuterWall)
                {
                    checkedVertices.Add(square.topLeft.vertexIndex);
                    checkedVertices.Add(square.topRight.vertexIndex);
                    checkedVertices.Add(square.bottomRight.vertexIndex);
                    checkedVertices.Add(square.bottomLeft.vertexIndex);
                }
                break;
        }
    }

    void MeshFromPoints(params Node[] points)
    {
        AssignVertices(points);

        if(points.Length >= 3)
            CreateTriangle(points[0], points[1], points[2]);
        if (points.Length >= 4)
            CreateTriangle(points[0], points[2], points[3]);
        if (points.Length >= 5)
            CreateTriangle(points[0], points[3], points[4]);
        if (points.Length >= 6)
            CreateTriangle(points[0], points[4], points[5]);
    }

    void AssignVertices(Node[] points)
    {
        for (int idx = 0; idx < points.Length; idx++)
        {
            if(points[idx].vertexIndex == -1)
            {
                points[idx].vertexIndex = vertices.Count;
                vertices.Add(points[idx].pos);
            }
        }
    }

    void CreateTriangle(Node a, Node b, Node c)
    {
        triangles.Add(a.vertexIndex);
        triangles.Add(b.vertexIndex);
        triangles.Add(c.vertexIndex);

        Triangle triangle = new Triangle(a.vertexIndex, b.vertexIndex, c.vertexIndex);

        for(int idx = 0; idx < 3; idx++)
        {
            AddTriangleToDict(triangle[idx], triangle);
        }
    }

    void AddTriangleToDict(int idxKey, Triangle triangle)
    {
        if (triangleDict.ContainsKey(idxKey))
        {
            triangleDict[idxKey].Add(triangle);
        }
        else
        {
            List<Triangle> triangles = new List<Triangle>();
            triangles.Add(triangle);
            triangleDict.Add(idxKey, triangles);
        }
    }

    void CalOutlines()
    {
        for(int idx = 0; idx < vertices.Count; idx++)
        {
            if(!checkedVertices.Contains(idx))
            {
                int newOutlineVertex = GetConnectedEdgeVertex(idx);
                if(newOutlineVertex != -1)
                {
                    checkedVertices.Add(idx);

                    List<int> newOutline = new List<int>();
                    newOutline.Add(idx);
                    outlines.Add(newOutline);
                    FollowOutline(newOutlineVertex, outlines.Count - 1);
                    outlines[outlines.Count - 1].Add(idx);
                }
            }
        }
    }

    void FollowOutline(int vertexIdx, int idx)
    {
        outlines[idx].Add(vertexIdx);
        checkedVertices.Add(vertexIdx);
        int newOutlineVertex = GetConnectedEdgeVertex(vertexIdx);
        if (newOutlineVertex != -1)
        {
            FollowOutline(newOutlineVertex, idx);
        }
    }

    int GetConnectedEdgeVertex(int vertexIdx)
    {
        List<Triangle> triangles = triangleDict[vertexIdx];
        
        for(int idx = 0; idx < triangles.Count; idx ++)
        {
            Triangle triangle = triangles[idx];
            for (int jdx = 0; jdx < 3; jdx++)
            {
                int vertexB = triangle[jdx];

                if (vertexIdx != vertexB && !checkedVertices.Contains(vertexB) && IsEdge(vertexIdx, vertexB) && triangle.IsOrderRight(vertexIdx, vertexB))
                {
                    return vertexB;
                }
            }
        }
        return -1;
    }

    bool IsEdge(int vertexA, int vertexB)
    {
        List<Triangle> trianglesHasA = triangleDict[vertexA];
        int trianglesCount = 0;
        for (int idx = 0; (idx < trianglesHasA.Count && trianglesCount <= 1); idx++)
        {
            if(trianglesHasA[idx].Contains(vertexB))
            {
                trianglesCount++;
            }
        }
        return trianglesCount == 1;
    }

    struct Triangle
    {
        int[] vertices;

        public Triangle(int a, int b, int c)
        {
            vertices = new int[3];
            vertices[0] = a;
            vertices[1] = b;
            vertices[2] = c;
        }

        public bool Contains(int idx)
        {
            return vertices[0] == idx || vertices[1] == idx || vertices[2] == idx;
        }

        public bool IsOrderRight(int a, int b)
        {
            int idxA = 0;
            int idxB = 0;
            for (int idx = 0; idx < 3; idx++)
            {
                if(vertices[idx] == a)
                {
                    idxA = idx;
                }
                if (vertices[idx] == b)
                {
                    idxB = idx;
                }
            }
            return idxB == idxA + 1 || (idxB == 0 && idxA == 2);
        }

        public int this[int i]
        {
            get
            {
                return vertices[i];
            }
        }
    }

    public class SquareGrid
    {
        public Square[,] squares;

        public SquareGrid(int[,] map, float squareSize)
        {
            int nodeCountX = map.GetLength(0);
            int nodeCountY = map.GetLength(1);
            float mapWidth = nodeCountX * squareSize;
            float mapHeight = nodeCountY * squareSize;

            ControlNode[,] controlNodes = new ControlNode[nodeCountX, nodeCountY];

            for (int x = 0; x < nodeCountX; x++)
            {
                for(int y = 0; y < nodeCountY; y++)
                {
                    Vector3 pos = new Vector3(x * squareSize - mapWidth / 2f + squareSize / 2f,
                                              0,
                                              y * squareSize - mapHeight / 2f + squareSize / 2f);
                    controlNodes[x, y] = new ControlNode(pos, map[x, y] == 1, squareSize);
                }
            }

            squares = new Square[nodeCountX - 1, nodeCountY - 1];
            for (int x = 0; x < nodeCountX - 1; x++)
            {
                for (int y = 0; y < nodeCountY - 1; y++)
                {
                    squares[x, y] = new Square(controlNodes[x, y + 1], controlNodes[x + 1, y + 1], controlNodes[x + 1, y], controlNodes[x, y]);
                    squares[x, y].isOuterWall = x == 0 || y == 0 || x == nodeCountX - 2 || y == nodeCountY - 2;
                }
            }
        }
    }

    public class Square
    {
        public ControlNode topLeft, topRight, bottomRight, bottomLeft;
        public Node centerTop, centerRight, centerBottom, centerLeft;
        public int configuration;
        public bool isOuterWall = false;

        public Square(ControlNode _topLeft, ControlNode _topRight, ControlNode _bottomRight, ControlNode _bottomLeft)
        {
            topLeft = _topLeft;
            topRight = _topRight;
            bottomRight = _bottomRight;
            bottomLeft = _bottomLeft;

            centerTop = topLeft.right;
            centerRight = _bottomRight.above;
            centerBottom = bottomLeft.right;
            centerLeft = bottomLeft.above;

            if (topLeft.isWall)
                configuration += 8;
            if (topRight.isWall)
                configuration += 4;
            if (bottomRight.isWall)
                configuration += 2;
            if (bottomLeft.isWall)
                configuration += 1;
        }
    }

    public class Node
    {
        public Vector3 pos;

        public int vertexIndex = -1;

        public Node(Vector3 _pos)
        {
            pos = _pos;
        }
    }

    public class ControlNode : Node
    {
        public Node above, right;

        public bool isWall;

        public ControlNode(Vector3 _pos, bool _isWall, float squareSize) : base(_pos)
        {
            isWall = _isWall;
            above = new Node(pos + Vector3.forward * squareSize / 2f);
            right = new Node(pos + Vector3.right * squareSize / 2f);
        }
    }
}
