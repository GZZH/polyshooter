﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

//[RequireComponent(typeof(Rigidbody))]
public class Enemy : LivingEntity
{
    public enum State {Idle, Chasing, Attacking}

    public LevelInfo[] levelInfo;

    public float attackRadious = 0.75f;
    public float attackCD = 1;//s
    public float attackSpeed = 3;
    public float attackDamage = 1;

    State currentState;
    Transform target;
    LivingEntity targetEntity;
    float nextAttackTime;
    float selfRadious;
    float targetRadious;
    float attackAngle = 25f;
    bool hasTarget = false;

    protected override void Awake()
    {
        base.Awake();
        isInvincible = true;
        currentState = State.Idle;
        SetInvincible(true);
    }

    public void Active()
    {
        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
            targetEntity = target.GetComponent<LivingEntity>();
            hasTarget = true;

            CapsuleCollider selfCollider = GetComponentInChildren<CapsuleCollider>();
            selfCollider.isTrigger = false;
            selfRadious = selfCollider.radius;
            targetRadious = target.GetComponentInChildren<BoxCollider>().size.x / 2.0f;

            GetComponent<AIPath>().target = target;
        }

        if (hasTarget)
        {
            targetEntity.OnDeath += OnTargetDeath;
            currentState = State.Chasing;
        }
        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.freezeRotation = true;
        SetInvincible(false);
    }

    public void SetLevel(int level, Color skinColor)
    {
        health = startingHealth * levelInfo[level].startHealthPrecent;
        attackDamage = attackDamage * levelInfo[level].attackDamagePrecent;
        GetComponent<AIPath>().speed *= levelInfo[level].moveSpeedPrecent;
        skinMaterial.color = skinColor;
        originalColor = skinColor;
        //print(levelInfo[level].startHealthPrecent);
    }

    void OnTargetDeath()
    {
        hasTarget = false;
        currentState = State.Idle;
    }

    protected override void Update ()
    {
        base.Update();
        if(!hasTarget && currentState != State.Idle)
        {
            currentState = State.Idle;
        }
        if(target != null && Time.time > nextAttackTime && currentState != State.Idle && hasTarget)
        {
            float sqrDisToTarget = (target.position - transform.position).sqrMagnitude;
            if (sqrDisToTarget < Mathf.Pow(attackRadious + targetRadious * 2, 2))
            {
                nextAttackTime = Time.time + attackCD;
                StartCoroutine("Attack");
            }
        }
    }

    IEnumerator Attack()
    {
        currentState = State.Attacking;
        transform.LookAt(new Vector3(target.position.x, transform.position.y, target.position.z));
        AudioManager.instance.PlaySound("EnemyAttack", transform.position);
        Vector3 originalSize = transform.localScale;
        //Vector3 attackSize = transform.localScale*1.05f;

        Vector3 originalDir = transform.eulerAngles;
        Vector3 finishAttackDir = transform.eulerAngles + new Vector3(attackAngle, 0, 0);
        Vector3 startAttackDir = transform.eulerAngles - new Vector3(attackAngle, 0, 0);
        //print(transform.forward * 10f);
        //Quaternion attackDir = Quaternion.LookRotation(target.position - transform.position * 1.2f);

        float percent = 0;
        bool hasDamaged = false;
        while(percent <= 1)
        {
            percent += Time.deltaTime * attackSpeed;
            //float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;//interpolation equation (-4x^2) + 4x

            float interpolation = 0;
            if (percent<0.5f)
            {
                interpolation = 2*Mathf.Pow(percent, 2) - 2*percent + 0.5f;//interpolation equation 2x^2-2x+0.5
            }else if (percent<0.75f)
            {
                interpolation = -128 * Mathf.Pow(percent - 0.5f, 3) + 48 * Mathf.Pow(percent - 0.5f, 2);//interpolation equation -128(x-0.5)^3+48x^2
            }else
            {
                interpolation = -8*Mathf.Pow(percent-0.75f, 2) + 1;//interpolation equation -8(x-0.75)^2+1
            }
            transform.localRotation = Quaternion.Euler(Vector3.Lerp(startAttackDir, finishAttackDir, interpolation));
            //print(interpolation);
            if (!hasDamaged && percent >= 0.75f && targetEntity != null)
            {
                float sqrDisToTarget = (target.position - transform.position).sqrMagnitude;
                if (sqrDisToTarget < Mathf.Pow(attackRadious + targetRadious + selfRadious, 2))
                {
                    //print(percent);
                    //print(interpolation);
                    hasDamaged = true;
                    targetEntity.TakeHit(attackDamage, targetEntity.transform.position, transform.forward);
                }
            }
            //print(Physics.CheckSphere(transform.position, 0.5f));
            //transform.localScale = Vector3.Lerp(originalSize, attackSize, interpolation);

            yield return null;
        }
        currentState = State.Chasing;
    }

    [System.Serializable]
    public class LevelInfo
    {
        public float moveSpeedPrecent = 1f;
        public float attackDamagePrecent = 1f;
        public float startHealthPrecent = 1f;
    }

    protected override void Die()
    {
        AudioManager.instance.PlaySound("EnemyDeath", transform.position);
        base.Die();
    }
}
