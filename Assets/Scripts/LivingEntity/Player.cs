﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(PlayerController))]
[RequireComponent(typeof(GunController))]
public class Player : LivingEntity
{
    public enum State { Idle, Running, Aiming, Moving}

    public CrossHair crossHair;

    public float moveSpeed = 5.0f;
    public float moveMultiple = 1.5f;
    public float runMultiple = 2.0f;
    public float aimMultiple = 0.5f;
    public float startViewRange = 20f;

    State currentState;

    Camera mainCamera;
    PlayerController controller;
    GunController gunController;
    Plane groundPlane;

    float viewRange;

    protected override void Awake()
    {
        base.Awake();
        viewRange = startViewRange;
        controller = GetComponent<PlayerController>();
        gunController = GetComponent<GunController>();
        mainCamera = Camera.main;
        groundPlane = new Plane(Vector3.up, Vector3.zero * gunController.GunHeight);
        crossHair.transform.rotation = mainCamera.transform.rotation;
        SetInvincible(true, 3f);
    }

    protected override void Update ()
    {
        base.Update();
        // Movement Input
        if (Input.GetAxisRaw("Fire2") == 1 && !gunController.IsReloading())
        {
            currentState = State.Aiming;
        }
        else if (Input.GetAxisRaw("Run") == 1 && !gunController.IsReloading() && !gunController.IsFiring())
        {
            currentState = State.Running;
        }
        else if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            currentState = State.Moving;
        }
        else
        {
            currentState = State.Idle;
        }

        Vector3 moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));

        // Third person moving;
        //Vector3 moveVelocity = (transform.forward * Input.GetAxisRaw("Vertical") +transform.right * Input.GetAxisRaw("Horizontal")).normalized* moveSpeed;

        float fixedSpeed = moveSpeed;
        float expandPrecentFixer;
        if (currentState == State.Running)
        {
            expandPrecentFixer = runMultiple;
            fixedSpeed *= runMultiple;
            viewRange = startViewRange;
        }
        else if (currentState == State.Moving)
        {
            expandPrecentFixer = moveMultiple;
            viewRange = startViewRange;
        }
        else if (currentState == State.Aiming)
        {
            expandPrecentFixer = aimMultiple;
            fixedSpeed *= aimMultiple;
            viewRange = startViewRange * (1f + aimMultiple);
        }
        else
        {
            expandPrecentFixer = 1f;
            viewRange = startViewRange;
        }
        Vector3 moveVelocity = moveInput.normalized * fixedSpeed;
        controller.Move(moveVelocity);

        // Look input
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        //print(mainCamera.gameObject);
        float rayDistance;

        if(groundPlane.Raycast(ray,out rayDistance))
        {
            Vector3 point = ray.GetPoint(rayDistance);
            if ((point - transform.position).sqrMagnitude > Mathf.Pow(viewRange, 2f))
            {
                Vector2 fixedPoint = new Vector2(point.x - transform.position.x, point.z - transform.position.z).normalized * viewRange;
                point = new Vector3(fixedPoint.x + transform.position.x, point.y, fixedPoint.y + transform.position.z);
            }
            //Debug.DrawLine(ray.origin, point, Color.red);
            controller.LookAt(point);
            if((new Vector2(point.x,point.z)-new Vector2(transform.position.x,transform.position.z)).sqrMagnitude > 1)
            {
                gunController.Aim(point);
            }
            crossHair.transform.position = point;
            crossHair.DetectTargets(ray);
        }
        gunController.SetFireDecayRange(expandPrecentFixer);
        crossHair.SetExpandPrecent(gunController.FireDecay);
        //crossHair.SetFireDecay(gunController.FireDecay);

        // WaeponInput
        if (Input.GetAxisRaw("Fire1") == 1)
        {
            gunController.OnTriggerHold();
        }
        else
        { 
            gunController.OnTriggerReleased();
        }
        if (Input.GetAxisRaw("Reload") == 1)
        {
            gunController.Reload();
        }
        if (Input.GetAxisRaw("GunSlot1") == 1)
        {
            gunController.SwitichGun(0);
        }
        if (Input.GetAxisRaw("GunSlot2") == 1)
        {
            gunController.SwitichGun(1);
        }
    }
    
    public float GetViewRange()
    {
        return viewRange;
    }

    public Vector2 GetAmmoInfo()
    {
        return gunController.GetAmmoInfo();
    }

    public bool AddHealth(float addedHealth)
    {
        if(health < startingHealth)
        {
            health = Mathf.Clamp(health + addedHealth, 0, startingHealth);
            return true;
        }
        return false;
    }

    public Vector2 GetHealthInfo()
    {
        Vector2 healthInfo = new Vector2(health, 0);
        return healthInfo;
    }

    protected override void Die()
    {
        AudioManager.instance.PlaySound("PlayerDeath", transform.position);
        base.Die();
    }
}
