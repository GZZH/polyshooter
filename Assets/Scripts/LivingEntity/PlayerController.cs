﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    Vector3 velocity;

	void Start ()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Move(Vector3 moveVelocity)
    {
        velocity = moveVelocity;
    }

    public void LookAt(Vector3 lookPoint)
    {
        Vector3 fixedPoint = new Vector3(lookPoint.x, transform.position.y, lookPoint.z);
        transform.LookAt(fixedPoint);
    }

    void FixedUpdate()
    {
        //Vector3.SmoothDamp(transform.position, targetPos, ref cameraSmoothVelocity, smoothTime);
        rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
    }
}
