﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingEntity : MonoBehaviour,IDamageable
{
    public ParticleSystem deathEffect;
    public ParticleSystem hitEffect;

    public float startingHealth;

    protected float injuredTimeLength = 0.1f;
    protected float invinciblTimeLength = 0.1f;
    protected float health;
    protected float injuredTime;
    protected bool dead = false;
    protected bool isInjured = false;
    protected bool isInvincible;
    protected Material skinMaterial;
    protected Color originalColor;

    float invincibleTime;

    public event System.Action OnDeath;
    protected virtual void Awake()
    {
        health = startingHealth;
        skinMaterial = GetComponentInChildren<Renderer>().material;
        originalColor = skinMaterial.color;
    }

    public virtual void TakeHit(float damage, Vector3 hitPoint, Vector3 hitDir)
    {
        if(!isInvincible)
        {
            AudioManager.instance.PlaySound("Impact", transform.position);
            if (damage >= health)
            {
                GameObject newDeathEffect = Instantiate(deathEffect.gameObject, hitPoint, Quaternion.FromToRotation(Vector3.forward, hitDir)) as GameObject;
                Destroy(newDeathEffect, deathEffect.main.startLifetimeMultiplier);
                newDeathEffect.GetComponent<ParticleSystemRenderer>().material.color = originalColor;
            }
            else
            {
                GameObject newHitEffect = Instantiate(hitEffect.gameObject, hitPoint, Quaternion.FromToRotation(Vector3.forward, hitDir)) as GameObject;
                Destroy(newHitEffect, hitEffect.main.startLifetimeMultiplier);
                newHitEffect.GetComponent<ParticleSystemRenderer>().material.color = originalColor;
            }
            TakeDamage(damage);
        }
    }

    public virtual void TakeDamage(float damage)
    {
        if(!isInvincible)
        {
            health -= damage;
            injuredTime = Time.time + injuredTimeLength;
            isInjured = true;
            if (skinMaterial != null)
            {
                skinMaterial.color = Color.red;
            }
            if (health <= 0 && !dead)
            {
                Die();
            }
        }
    }

    public virtual void SetInvincible(bool input, float _invincibleTime = 0f)
    {
        isInvincible = input;
        invincibleTime = _invincibleTime;
        if (isInvincible)
        {
            StartCoroutine("InvincibleFlash");
        }
        else
        {
            StopCoroutine("InvincibleFlash");
            skinMaterial.color = originalColor;
        }
    }

    public virtual bool IsInvincible()
    {
        return isInvincible;
    }

    IEnumerator InvincibleFlash()
    {
        bool isOriginalColor = true;
        float startTime = Time.time;
        while (isInvincible)
        {
            if(isOriginalColor)
            {
                skinMaterial.color = (Color.white + originalColor) / 2f;
                isOriginalColor = false;
            }
            else
            {
                skinMaterial.color = originalColor;
                isOriginalColor = true;
            }
            if (invincibleTime > 0f && Time.time >= startTime + invincibleTime)
            {
                skinMaterial.color = originalColor;
                isInvincible = false;
            }
            yield return new WaitForSeconds(invinciblTimeLength);
        }
    }

    [ContextMenu("Suicide")]
    protected virtual void Die()
    {
        dead = true;
        if (OnDeath != null)
        {
            OnDeath();
        }
        if(gameObject != null)
        {
            Destroy(gameObject);
        }
    }

    protected virtual void Update()
    {
        if (isInjured && Time.time > injuredTime)
        {
            isInjured = false;
            skinMaterial.color = originalColor;
        }

        if (transform.position.y < -10)
        {
            TakeDamage(health);
        }
    }
}
