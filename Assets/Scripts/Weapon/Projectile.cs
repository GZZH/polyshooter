﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public LayerMask collisionMask;
    public Color traileColor;

    public float damage = 1;

    float speed = 10;
    float lifeTime = 3.0f;//s
    float rayDeviation = .1f;

    private void Start()
    {
        Destroy(gameObject, lifeTime);
        //print(collisionMask.value);
        Collider[] initialCollisions = Physics.OverlapSphere(transform.position,0.1f, collisionMask);
        if (initialCollisions.Length > 0)
        {
            OnHitObject(initialCollisions[0], transform.position);
        }
        GetComponent<TrailRenderer>().material.SetColor("_TintColor", traileColor);
    }
    public void SetSpeed (float newSpeed)
    {
        speed = newSpeed;
    }

	void Update ()
    {
        float moveDistance = Time.deltaTime * speed;
        CheckCollisions(moveDistance);
        transform.Translate(Vector3.forward * moveDistance);
	}

    void CheckCollisions(float moveDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, moveDistance + rayDeviation, collisionMask, QueryTriggerInteraction.Collide))
        {
            OnHitObject(hit.collider, hit.point);
        }
    }

    void OnHitObject(Collider collider, Vector3 hitPoint)
    {
        IDamageable damageableObject = collider.GetComponentInParent<IDamageable>();
        if (damageableObject != null)
        {
            damageableObject.TakeHit(damage, hitPoint, transform.forward);
        }
        Destroy(gameObject);
    }

    public void SetDamage(float _damage)
    {
        damage = _damage;
    }
}
