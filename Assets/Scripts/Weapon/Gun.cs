﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public enum FireMode { Auto, Burst, Single };

    [Header("----------GunProperty----------")]
    public FireMode fireMode;

    public Mag[] mags;

    public Transform muzzle;
    public Projectile projectile;

    public bool infiniteAmmo;
    public bool noMag;
    public float shotCD = 100; //ms
    public float muzzleVelocity = 35;
    public float projectileDamage = 1;
    public float reloadTime = 1f;
    public float reloadPosAngle = 30f;
    public int burstCount = 1;
    public int projectilesPerShoot = 1;
    public int projectilesPerMag = 10;
    public int projectilesInBarrel = 1;
    public int totalProjectiles = 100;

    float lastFiredTime;
    int burstShotsRemain;
    int projectilesInGun;
    int totalAmmoRemain;
    int reloadingStep;
    bool triggerReleased = true;
    bool isFiring;
    bool isReloading;

    [Header("----------Recoil----------")]
    public Vector2 recoilBack = new Vector2(0.1f, 0.3f);
    public Vector2 recoilUp = new Vector2(5, 10);

    public float recoilTime = 0.1f;

    Vector2 recoilUpClamp = new Vector2(0, 30);
    Vector3 recoilBackVelocity;

    float recoilAngle;
    float recoilUpVelocity;

    [Header("----------FireDecay----------")]
    public float spreadRange = 1.7f;
    public float baseSpreadPrecent = 1f;
    public float fireDecayAmount = 0.1f;// %/shot
    public float coolDownAmount = 0.5f;// %/s

    Vector2 fireDecayRange;
    //float fireDecayMaxPrecent = 3; //300%
    //float coolDownDelay = 0.25f;
    float maxSpreadPrecent;
    float fireDecayPrecent;
    float fireDecayCheckTime;
    //bool isCooling;

    [Header("----------Effects----------")]
    public AudioClip[] shootAudios;

    public AudioClip clipoutAudio;
    public AudioClip clipinAudio;
    public AudioClip boltslapAudio;
    public Transform shell;
    public Transform shellEjection;

    Mag[] tempMags;

    MuzzleFire muzzleFlash;


    private void Start()
    {
        muzzleFlash = GetComponent<MuzzleFire>();
        burstShotsRemain = burstCount;
        projectilesInGun = projectilesPerMag;
        totalAmmoRemain = totalProjectiles;
        maxSpreadPrecent = 3 * baseSpreadPrecent;
        fireDecayRange = new Vector2(baseSpreadPrecent, maxSpreadPrecent);
        tempMags = new Mag[mags.Length];
    }

    public void SetFireDecayRange (float percent)
    {
        fireDecayRange.x = baseSpreadPrecent * percent;
        fireDecayRange.y = maxSpreadPrecent * percent;
    }
    
    public void CalFireDecayPrecent()
    {
        /*if (isCooling || Time.time > lastFiredTime + coolDownDelay)
        {
            isCooling = true;
            fireDecayPrecent -= (Time.time - fireDecayCheckTime) * coolDownAmount;
        }*/
        fireDecayPrecent -= (Time.time - fireDecayCheckTime) * coolDownAmount;
        fireDecayPrecent = Mathf.Clamp(fireDecayPrecent, fireDecayRange.x, fireDecayRange.y);
        fireDecayCheckTime = Time.time;
    }

    IEnumerator FireController()
    {
        isFiring = true;
        while (fireMode == FireMode.Burst && burstShotsRemain > 0)
        {
            if (Time.time > lastFiredTime + shotCD / 1000) //s
            {
                burstShotsRemain--;
                Shoot();
            }
            else
            {
                yield return null;
            }
        }
        burstShotsRemain = burstCount;
        while (fireMode == FireMode.Auto && !triggerReleased)
        {
            if (Time.time > lastFiredTime + shotCD / 1000 && projectilesInGun > 0) //s
            {
                Shoot();
            }
            else
            {
                yield return null;
            }
        }
        if (fireMode == FireMode.Single && triggerReleased && Time.time > lastFiredTime + shotCD / 1000) //s
        {
            Shoot();
        }
        isFiring = false;
    }

    public void StopFireController()
    {
        StopCoroutine("FireController");
        isFiring = false;
    }

    void Shoot()
    {
        if(noMag || projectilesInGun > 0)
        {
            AudioManager.instance.PlaySound(shootAudios[Random.Range(0, shootAudios.Length)], muzzle.position);
            for (int i = 0; i < projectilesPerShoot; i++)
            {
                CalFireDecayPrecent();
                float fireDecay = fireDecayPrecent;
                lastFiredTime = Time.time;

                Vector3 fixedSpreadVector = Quaternion.Euler(Random.Range(-spreadRange * fireDecay, spreadRange * fireDecay),
                                                             Random.Range(-spreadRange * fireDecay, spreadRange * fireDecay),
                                                             Random.Range(-spreadRange * fireDecay, spreadRange * fireDecay)) * muzzle.forward;

                Quaternion projectileDir = Quaternion.LookRotation(fixedSpreadVector);

                Projectile newProjectile = Instantiate(projectile, muzzle.position, projectileDir) as Projectile;
                newProjectile.SetSpeed(muzzleVelocity);
                newProjectile.SetDamage(projectileDamage);

                fireDecayPrecent += fireDecayAmount;
            }
            if (shell != null)
            {
                Instantiate(shell, shellEjection.position, shellEjection.rotation);
            }
            muzzleFlash.Activate();
            if (!noMag)
            {
                projectilesInGun--;
            }
            //isCooling = false;

            Recoil();
        }
    }

    void Recoil()
    {
        transform.localPosition -= Vector3.forward * Random.Range(recoilBack.x, recoilBack.y);

        recoilAngle += Random.Range(recoilUp.x, recoilUp.y); ;
        recoilAngle = Mathf.Clamp(recoilAngle, recoilUpClamp.x, recoilUpClamp.y);
    }

    IEnumerator AnimateReload()
    {
        isReloading = true;

        float speed = 1f;
        float percent = 0;
        bool startReload = false;
        Vector3 initRotation = transform.localEulerAngles;

        while (percent < 1)
        {
            percent += Time.deltaTime * speed;
            float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
            float reloadAngle = Mathf.Lerp(0, reloadPosAngle, interpolation);
            transform.localEulerAngles = new Vector3(-1,-1,0) * reloadAngle;

            if (percent >= 0.5f && !startReload)
            {
                startReload = true;
                if (reloadingStep <= 0)
                {
                    reloadingStep = 0;
                    AudioManager.instance.PlaySound(clipoutAudio, transform.position);
                    int projectilesLeft = Mathf.Clamp(projectilesInGun, 0, projectilesInBarrel);
                    totalAmmoRemain += projectilesInGun - projectilesLeft;
                    projectilesInGun = projectilesLeft;
                    for (int i = 0; i < mags.Length; i++)
                    {
                        tempMags[i] = Instantiate(mags[i], mags[i].transform.position, mags[i].transform.rotation, transform);
                        tempMags[i].gameObject.SetActive(false);
                        mags[i].Changed();
                    }
                }
                if (reloadingStep <= 1)
                {
                    reloadingStep = 1;
                    yield return new WaitForSeconds(reloadTime);
                    AudioManager.instance.PlaySound(clipinAudio, transform.position);
                    for (int i = 0; i < tempMags.Length; i++)
                    {
                        mags[i] = tempMags[i];
                        mags[i].gameObject.SetActive(true);
                    }
                }
                if (reloadingStep <= 2)
                {
                    reloadingStep = 2;
                    if (projectilesInGun == 0)
                    {
                        yield return new WaitForSeconds(reloadTime);
                        AudioManager.instance.PlaySound(boltslapAudio, transform.position);
                    }
                }
                if (reloadingStep <= 2)
                {
                    reloadingStep = 2;
                    if (projectilesPerMag <= totalAmmoRemain || infiniteAmmo)
                    {
                        projectilesInGun += projectilesPerMag;
                        totalAmmoRemain -= projectilesPerMag;
                    }
                    else
                    {
                        projectilesInGun += totalAmmoRemain;
                        totalAmmoRemain -= totalAmmoRemain;
                    }
                    totalAmmoRemain = Mathf.Clamp(totalAmmoRemain, 0, totalAmmoRemain);
                    reloadingStep = 0;
                }
            }
            yield return null;
        } 
        isReloading = false;
    }

    public void Reload()
    {
        if (!isReloading && projectilesInGun < projectilesPerMag + projectilesInBarrel && (totalAmmoRemain > 0 || infiniteAmmo) && !noMag && !isFiring)
        {
            StartCoroutine("AnimateReload");
        }
    }

    public void StopReload()
    {
        if(isReloading)
        {
            StopCoroutine("AnimateReload");
            isReloading = false;
            transform.localEulerAngles = Vector3.zero;
        }
    }

    public bool IsReloading()
    {
        return isReloading;
    }

    public bool IsFiring()
    {
        return isFiring;
    }

    void Update()
    {
        CalFireDecayPrecent();
        if (!isReloading)
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, Vector3.zero, ref recoilBackVelocity, recoilTime);

            recoilAngle = Mathf.SmoothDamp(recoilAngle, 0, ref recoilUpVelocity, recoilTime);
            transform.localEulerAngles = Vector3.left * recoilAngle;
        }
    }

    public void OnTriggerHold()
    {
        if (triggerReleased && projectilesInGun <= 0 && !isReloading && (totalAmmoRemain > 0 || infiniteAmmo))
        {
            Reload();
        }
        if (triggerReleased && !isFiring && !isReloading)
        {
            if (fireMode == FireMode.Auto)
            {
                triggerReleased = false;
            }
            StartCoroutine("FireController");
        }
        triggerReleased = false;
    }

    public void OnTriggerReleased()
    {
        triggerReleased = true;
    }

    public Vector2 GetAmmoInfo()
    {
        Vector2 AmmoInfo = new Vector2(projectilesInGun, totalAmmoRemain);
        if (noMag)
        {
            AmmoInfo.x = -1;
        }
        if (infiniteAmmo)
        {
            AmmoInfo.y = -1;
        }
        return AmmoInfo;
    }

    public float FireDecay
    {
        get
        {
            return fireDecayPrecent;
        }
    }

    public bool AddAmmo(float percent)
    {
        int totalAmmo = totalProjectiles + projectilesPerMag;
        if(infiniteAmmo || totalAmmoRemain + projectilesInGun == totalAmmo)
        {
            return false;
        }
        int addAmmoAmount = (int)Mathf.Clamp(percent * totalAmmo, 1, totalAmmo);
        totalAmmoRemain += addAmmoAmount;
        totalAmmoRemain = Mathf.Clamp(totalAmmoRemain, 0, totalAmmo - projectilesInGun);
        return true;
    }
}
