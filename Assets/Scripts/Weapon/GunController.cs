﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    public Transform weaponHold;
    public Transform carryHold;
    public Gun[] startingGuns;

    Gun[] guns = new Gun[2];
    Gun equipedGun;

    int equipedGunIdx = 0;

    private void Start()
    {
        foreach(Gun gun in startingGuns)
        {
            if (gun != null)
            {
                EquipGun(gun);
            }
        }
    }

    public void EquipGun(Gun gunToEquip)
    {
        Gun newGun = Instantiate(gunToEquip, weaponHold.position, weaponHold.rotation) as Gun;
        if(equipedGun == null)
        {
            guns[equipedGunIdx] = newGun;
            equipedGun = guns[equipedGunIdx];
            equipedGun.transform.parent = weaponHold;
            return;
        }
        for(int idx = 0; idx < guns.Length; idx++)
        {
            if (guns[idx] == null)
            {
                guns[idx] = newGun;
                newGun.transform.parent = carryHold;
                return;
            }
        }
        Destroy(guns[equipedGunIdx]);
        guns[equipedGunIdx] = newGun;
        equipedGun = guns[equipedGunIdx];
        equipedGun.transform.parent = weaponHold;
    }

    public void SwitichGun(int idx)
    {
        if(equipedGunIdx != idx)
        {
            equipedGun.StopFireController();
            equipedGun.StopReload();
            equipedGunIdx = idx;
            equipedGun.transform.parent = carryHold;
            equipedGun = guns[equipedGunIdx];
            equipedGun.transform.parent = weaponHold;
        }
    }

    public void Aim(Vector3 lookPoint)
    {
        Vector3 fixedPoint = new Vector3(lookPoint.x, weaponHold.position.y, lookPoint.z);
        weaponHold.LookAt(fixedPoint);
    }

    public void SetFireDecayRange(float percent)
    {
        if (equipedGun != null)
        {
            equipedGun.SetFireDecayRange(percent);
        }
    }

    public void Reload()
    {
        if (equipedGun != null)
        {
            equipedGun.Reload();
        }
    }

    public bool IsReloading()
    {
        if (equipedGun != null)
        {
            return equipedGun.IsReloading();
        }
        return false;
    }

    public bool IsFiring()
    {
        if (equipedGun != null)
        {
            return equipedGun.IsFiring();
        }
        return false;
    }

    public void OnTriggerHold()
    {
        if (equipedGun != null)
        {
            equipedGun.OnTriggerHold();
        }
    }

    public void OnTriggerReleased()
    {
        if (equipedGun != null)
        {
            equipedGun.OnTriggerReleased();
        }
    }

    public Vector2 GetAmmoInfo()
    {
        if (equipedGun != null)
        {
            return equipedGun.GetAmmoInfo();
        }
        return new Vector2(-1,-1);
    }

    public float GunHeight
    {
        get
        {
            return weaponHold.position.y;
        }
    }

    public float FireDecay
    {
        get
        {
            return equipedGun.FireDecay;
        }
    }

    public bool AddAmmo(float percent)
    {
        bool returning = false;
        foreach(Gun gun in guns)
        {
            if(gun != null)
            {
                returning = (returning || gun.AddAmmo(percent));
            }
        }
        return returning;
    }
}
