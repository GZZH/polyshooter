﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mag : MonoBehaviour
{
    public float minForce;
    public float maxForce;

    Rigidbody rb;
    Collider colider;

    float lifeTime = 4;
    float fadeTime = 2;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        colider = GetComponent<Collider>();
    }

    public void Changed()
    {
        transform.SetParent(null);
        rb.isKinematic = false;
        colider.isTrigger = false;
        float force = Random.Range(minForce, maxForce);
        rb.AddForce(transform.right * force);
        rb.AddTorque(Random.insideUnitSphere * force);
        StartCoroutine(Fade());
    }

    IEnumerator Fade()
    {
        yield return new WaitForSeconds(lifeTime);

        float percent = 0;
        float fadeSpeed = 1 / fadeTime;
        Material mat = GetComponent<Renderer>().material;
        Color initColor = mat.color;

        while (percent < 1)
        {
            percent += Time.deltaTime * fadeSpeed;
            mat.color = Color.Lerp(initColor, Color.clear, percent);
            yield return null;
        }
        Destroy(gameObject);
    }
}
