﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    public Map[] maps;
    public int mapIdx;

    public Transform tilePrefab;
    public Transform obstaclePrefab;
    public Transform mapFloor;
    public Transform navmeshFloor;
    public Transform navmeshMaskPrefab;
    public Vector2 maxMapSize;

    [Range(0,1)]
    public float outLinePrecent;

    public float tileSize;

    Transform[,] tileMap;

    List<Coord> allTileCoords;
    Queue<Coord> shuffledTileCoords;
    Queue<Coord> shuffledOpenTileCoords;

    Map currentMap;

    public void Awake()
    {
        FindObjectOfType<GameManager>().OnNewWave += OnNewWave;
        Generatemap();
    }

    void OnNewWave(int waveNum)
    {
        if(waveNum <= maps.Length)
        {
            mapIdx = waveNum - 1;
        }
        else
        {
            currentMap.seed = Random.Range(0, 100);
        }
        //mapIdx = waveNum - 1;
        print(mapIdx);
        Generatemap();
    }

    public void Generatemap()
    {
        currentMap = maps[mapIdx];
        tileMap = new Transform[currentMap.mapSize.x, currentMap.mapSize.y];
        System.Random ranNumGen = new System.Random(currentMap.seed);

        //Generating Coords
        allTileCoords = new List<Coord>();
        for (int x = 0; x < currentMap.mapSize.x; x++)
        {
            for (int y = 0; y < currentMap.mapSize.y; y++)
            {
                allTileCoords.Add(new Coord(x, y));
            }
        }
        shuffledTileCoords = new Queue<Coord>(Utility.ShuffleArray(allTileCoords.ToArray(), currentMap.seed));

        //Creates map holder object
        string holderName = "Generated Map";
        if (transform.Find(holderName))
        {
            DestroyImmediate(transform.Find(holderName).gameObject);
        }

        Transform mapHolder = new GameObject(holderName).transform;
        mapHolder.SetParent(transform);

        //Spawning Tiles;
        for (int x = 0; x < currentMap.mapSize.x; x++)
        {
            for (int y = 0; y < currentMap.mapSize.y; y++)
            {
                Vector3 tilePosition = CoordToPosition(x, y);
                Transform newTile = Instantiate(tilePrefab, tilePosition, Quaternion.Euler(Vector3.right*90)) as Transform;
                newTile.localScale = Vector3.one * (1 - outLinePrecent) * tileSize;
                newTile.SetParent(mapHolder);
                tileMap[x, y] = newTile;
            }
        }

        //Spawning obstacles
        bool[,] obstacleMap = new bool[(int)currentMap.mapSize.x,(int)currentMap.mapSize.y];
            
        int obstacleCount = (int)(currentMap.obstaclePrecent * currentMap.mapSize.x * currentMap.mapSize.y);
        int currentObstacleCount = 0;

        List<Coord> allOpenCoords = new List<Coord>(allTileCoords);

        for (int i = 0; i < obstacleCount; i++)
        {
            Coord randomCoord = GetRanCoord();
            obstacleMap[randomCoord.x, randomCoord.y] = true;
            currentObstacleCount++;

            if (((randomCoord.x != 0 && randomCoord.y != 0 && randomCoord.x != currentMap.mapSize.x -1 && randomCoord.y != currentMap.mapSize.y - 1) || (float)ranNumGen.NextDouble() < currentMap.outerObstaclePrecent) 
                && randomCoord != currentMap.mapCentre && MapIsFullyAccessible(obstacleMap, currentObstacleCount))
            {
                float obstacleHeight = Mathf.Lerp(currentMap.minObstacleHeight, currentMap.maxObstacleHeight, (float)ranNumGen.NextDouble());
                Vector3 obstaclePosition = CoordToPosition(randomCoord.x, randomCoord.y);

                Transform newObstacle = Instantiate(obstaclePrefab, obstaclePosition + Vector3.up * obstacleHeight/2f, Quaternion.identity) as Transform;
                newObstacle.localScale = new Vector3((1 - outLinePrecent) * tileSize, obstacleHeight, (1 - outLinePrecent) * tileSize);
                newObstacle.SetParent(mapHolder);
                Renderer obstacleRenderer = newObstacle.GetComponent<Renderer>();
                Material obstacleMaterial = new Material(obstacleRenderer.sharedMaterial);
                float colorPercent = (float)randomCoord.y / (float)currentMap.mapSize.y;
                obstacleMaterial.color = Color.Lerp(currentMap.foregroundColor, currentMap.backgroundColor, colorPercent);
                obstacleRenderer.sharedMaterial = obstacleMaterial;
                allOpenCoords.Remove(randomCoord);
            }
            else
            {
                obstacleMap[randomCoord.x, randomCoord.y] = false;
                currentObstacleCount--;
            }
        }

        shuffledOpenTileCoords = new Queue<Coord>(Utility.ShuffleArray(allOpenCoords.ToArray(), currentMap.seed));

        //Creating navmashMask
        Transform maskLeft = Instantiate(navmeshMaskPrefab, Vector3.left * tileSize * (currentMap.mapSize.x + maxMapSize.x) / 4f, Quaternion.identity) as Transform;
        maskLeft.localScale = new Vector3((maxMapSize.x - currentMap.mapSize.x) / 2f, 1, currentMap.mapSize.y) * tileSize;
        maskLeft.SetParent(mapHolder);

        Transform maskRight = Instantiate(navmeshMaskPrefab, Vector3.right * tileSize * (currentMap.mapSize.x + maxMapSize.x) / 4f, Quaternion.identity) as Transform;
        maskRight.localScale = new Vector3((maxMapSize.x - currentMap.mapSize.x) / 2f, 1, currentMap.mapSize.y) * tileSize;
        maskRight.SetParent(mapHolder);

        Transform maskTop = Instantiate(navmeshMaskPrefab, Vector3.forward * tileSize * (currentMap.mapSize.y + maxMapSize.y) / 4f, Quaternion.identity) as Transform;
        maskTop.localScale = new Vector3(maxMapSize.x, 1, (maxMapSize.y - currentMap.mapSize.y) / 2f) * tileSize;
        maskTop.SetParent(mapHolder);

        Transform maskBottom = Instantiate(navmeshMaskPrefab, Vector3.back * tileSize * (currentMap.mapSize.y + maxMapSize.y) / 4f, Quaternion.identity) as Transform;
        maskBottom.localScale = new Vector3(maxMapSize.x, 1, (maxMapSize.y - currentMap.mapSize.y) / 2f) * tileSize;
        maskBottom.SetParent(mapHolder);

        navmeshFloor.localScale = new Vector3(maxMapSize.x, maxMapSize.y) * tileSize;
        mapFloor.localScale = new Vector3(currentMap.mapSize.x * tileSize, currentMap.mapSize.y * tileSize);
    }

    bool MapIsFullyAccessible(bool[,] obstacleMap, int currentObstacleCount)
    {
        bool[,] mapFlags = new bool[obstacleMap.GetLength(0), obstacleMap.GetLength(1)];
        Queue<Coord> queue = new Queue<Coord>();
        queue.Enqueue(currentMap.mapCentre);
        mapFlags[currentMap.mapCentre.x, currentMap.mapCentre.y] = true;

        int accessibleTileCount = 1;

        while (queue.Count > 0)
        {
            Coord tile = queue.Dequeue();
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    int neighbourX = tile.x + x;
                    int neighbourY = tile.y + y;
                    if ((x == 0|| y == 0)&&
                        (neighbourX >= 0 && neighbourY >=0 && neighbourX < obstacleMap.GetLength(0) && neighbourY < obstacleMap.GetLength(1)) &&
                        (!mapFlags[neighbourX, neighbourY] && !obstacleMap[neighbourX, neighbourY]))
                    {
                        mapFlags[neighbourX, neighbourY] = true;
                        queue.Enqueue(new Coord(neighbourX, neighbourY));
                        accessibleTileCount++;
                    }
                }
            }
        }

        int targetAccessibleTileCount = (int) (currentMap.mapSize.x * currentMap.mapSize.y - currentObstacleCount);

        return targetAccessibleTileCount == accessibleTileCount;
    }

    Vector3 CoordToPosition(int x, int y)
    {
        return new Vector3(-currentMap.mapSize.x / 2f + 0.5f + x, 0, -currentMap.mapSize.y / 2f + 0.5f + y) * tileSize;
    }

    public Transform GetTileFromPos(Vector3 pos)
    {
        int x = Mathf.RoundToInt(pos.x / tileSize + (currentMap.mapSize.x - 1) / 2f);
        int y = Mathf.RoundToInt(pos.z / tileSize + (currentMap.mapSize.y - 1) / 2f);

        x = Mathf.Clamp(x, 0, tileMap.GetLength(0) - 1);
        y = Mathf.Clamp(y, 0, tileMap.GetLength(1) - 1);

        return tileMap[x, y];
    }

    public Coord GetRanCoord()
    {
        Coord randomCoord = shuffledTileCoords.Dequeue();
        shuffledTileCoords.Enqueue(randomCoord);
        return randomCoord;
    }

    public Transform GetRanOpenTile()
    {
        Coord randomCoord = shuffledOpenTileCoords.Dequeue();
        shuffledOpenTileCoords.Enqueue(randomCoord);
        return tileMap[randomCoord.x, randomCoord.y];
    }

    [System.Serializable]
    public struct Coord
    {
        public int x;
        public int y;

        public Coord(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public static bool operator == (Coord c1, Coord c2f)
        {
            return c1.x == c2f.x && c1.y == c2f.y;
        }

        public static bool operator != (Coord c1, Coord c2f)
        {
            return c1.x != c2f.x || c1.y != c2f.y;
        }
    }

    [System.Serializable]
    public class Map
    {
        public Coord mapSize;
        public Color foregroundColor;
        public Color backgroundColor;

        [Range(0, 1)]
        public float obstaclePrecent;
        [Range(0, 1)]
        public float outerObstaclePrecent;

        public int seed;
        public float minObstacleHeight;
        public float maxObstacleHeight;

        public Coord mapCentre
        {
            get
            {
                return new Coord(mapSize.x / 2, mapSize.y / 2);
            }
        }

          
    }
}
