﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MapGenerator2))]
public class MapGenerator2Editor : Editor
{
    public override void OnInspectorGUI()
    {
        MapGenerator2 map = target as MapGenerator2;

        if (DrawDefaultInspector() ||
            GUILayout.Button("Generate Map"))
        {
            map.GenerateMap();
        }
    }
}
