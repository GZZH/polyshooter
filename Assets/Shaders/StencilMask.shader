﻿Shader "Custom/StencilMask" {
    Properties {
        _Color ("Main Color", Color) = (1,1,1,0)
        _SpecColor ("Spec Color", Color) = (1,1,1,1)
        _Emission ("Emmisive Color", Color) = (0,0,0,0)
        _Shininess ("Shininess", Range (0.01, 1)) = 0.7
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    SubShader {
    Tags { "RenderType"="Opaque" "Queue"="Geometry-1"}
    	
        Pass {
    	//关闭RGBA输出通道
    	ColorMask 0
    	//关闭深度写入
    	ZWrite Off
    			
    	//设置模板测试
    	Stencil{
    	//参考值
    	Ref 1
    	//总是通过
    	Comp Always
    	//模板测试和深度测试都成功后，用Ref值替换掉stencilBuffer值
    	    Pass Replace
    	}
    		
            Material {
                //漫反射颜色
                Diffuse [_Color]
                //环境光颜色
                Ambient [_Color]
                //亮度
                Shininess [_Shininess]
                //高光颜色
                Specular [_SpecColor]
                //自发光颜色
                Emission [_Emission]
            }
            //开启光照
            Lighting On
            //开启高光
            SeparateSpecular On
            //混合纹理
            SetTexture [_MainTex] {
                //纹理混合模式参见: https://docs.unity3d.com/Manual/SL-SetTexture.html
                //主纹理rgb * 光照颜色rgb 2倍亮度, 主纹理alpha * 光照颜色的alpha
                Combine texture * primary DOUBLE, texture * primary
     
                //Quad: 4倍亮度
                //Combine texture * primary Quad, texture * primary
            }
        }
    }
}